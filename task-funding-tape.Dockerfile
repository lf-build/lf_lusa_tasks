FROM registry.lendfoundry.com/base:beta8

ADD ./src/LendFoundry.Tasks.Abstractions /app/LendFoundry.Tasks.Abstractions
WORKDIR /app/LendFoundry.Tasks.Abstractions
RUN eval "$CMD_RESTORE"

ADD ./src/LendFoundry.Tasks.Agent /app/LendFoundry.Tasks.Agent
WORKDIR /app/LendFoundry.Tasks.Agent
RUN eval "$CMD_RESTORE"

ADD ./src/LendFoundry.Tasks.Loans.LoanTape /app/LendFoundry.Tasks.Loans.LoanTape
WORKDIR /app/LendFoundry.Tasks.Loans.LoanTape
RUN eval "$CMD_RESTORE"
RUN dnu build

ENTRYPOINT dnx run