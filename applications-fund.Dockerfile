FROM registry.lendfoundry.com/base:beta8

ENV MONO_THREADS_PER_CPU 2000
ADD ./src/LendFoundry.Tasks.Abstractions /app/LendFoundry.Tasks.Abstractions
ADD ./src/LendFoundry.Tasks.Agent /app/LendFoundry.Tasks.Agent
ADD ./src/LendFoundry.Tasks.Applications.Fund /app/LendFoundry.Tasks.Applications.Fund

WORKDIR /app/LendFoundry.Tasks.Abstractions
RUN eval "$CMD_RESTORE"
RUN dnu build

WORKDIR /app/LendFoundry.Tasks.Agent
RUN eval "$CMD_RESTORE"
RUN dnu build

WORKDIR /app/LendFoundry.Tasks.Applications.Fund
RUN eval "$CMD_RESTORE"
RUN dnu build

ENTRYPOINT dnx run