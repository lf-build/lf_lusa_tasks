﻿using LendFoundry.Applications.Filters;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile.Tests
{
    public class CreateOnBoardingApplicationsTests : InMemoryObjects
    {
        public CreateOnBoardingApplicationsTests()
        {
            ConfigurationFactory.Setup(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()))
                .Returns(ConfigurationService.Object);

            TokenHandler.Setup(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Mock.Of<IToken>());

            TenantTimeFactory.Setup(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()))
                .Returns(TenantTime);

            EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(EventHubClient.Object);
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            LoggerFactory.Setup(x => x.CreateLogger())
                .Returns(Logger.Object);

            StatusManagementServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
             .Returns(StatusManagementService.Object);

            ApplicationFilterServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(ApplicationFilterService.Object);
            ApplicationServiceClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
               .Returns(ApplicationService.Object);

            MerchantCacheServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(MerchantCacheService.Object);
            DocumentManagerServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
               .Returns(DocumentManagerService);
            DataAttributesClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
             .Returns(DataAttributesEngine.Object);
            FileStorageFactory.Setup(x => x.Create(It.IsAny<FileStorage>(), Logger.Object))
            .Returns(FileStorageService);
        }

        private Agent Agent => new Agent
        (
            ConfigurationFactory.Object,
            TokenHandler.Object,
            EventHubFactory.Object,
            TenantTimeFactory.Object,
            LoggerFactory.Object,
           CsvGenerator,
            StatusManagementServiceFactory.Object,
            ApplicationFilterServiceFactory.Object,
             ApplicationServiceClientFactory.Object,

            MerchantCacheServiceFactory.Object,
            DocumentManagerServiceFactory.Object,
            DataAttributesClientFactory.Object,
            FileStorageFactory.Object
        );

        protected Mock<ISettingReader> SettingReader { get; } = new Mock<ISettingReader>();
        protected Dictionary<string, string> CsvProjection { get; } = new Dictionary<string, string> {
                { "Fund", "Fund" },
                { "AssetType", "Asset Type" },
                {  "Merchant", "Merchant"},
                { "InternalAccountNumber", "Internal Account #"},
                { "BorrowerSsn", "Borrower SSN"},
                { "BorrowerLastName", "Borrower Last Name"},
                { "BorrowerFirstName", "Borrower First Name" },
                { "Dob", "DOB"},
                { "BorrowerAddr", "Borrower Addr"},
                { "BorrowerAddrCity", "Borrower Addr City"                                                          },
                { "BorrowerAddrState", "Borrower Addr State"                                                        },
                { "BorrowerAddrZipCode", "Borrower Addr Zip Code"                                                  },
                { "BorrowerPhone1", "Borrower Phone 1"                                                            },
                { "BorrowerPhone2", "Borrower Phone 2"                                                              },
                { "BorrowerPhone3", "Borrower Phone 3"                                                             },
                { "BorrowerEmailAddress", "Borrower Email Address"                                                 },
                { "CoBorrowerLastName", "Co Borrower Last Name"                                                    },
                { "CoBorrowerFirstName", "Co Borrower First Name"                                                    },
                { "CoBorrowerSsn", "Co Borrower SSN"                                                               },
                { "CoBorrowerDob", "Co Borrower DOB"                                                                },
                { "CoBorrowerAddr", "Co Borrower Addr"                                                              },
                { "CoBorrowerAddrCity", "Co Borrower Addr City"                                                     },
                { "CoBorrowerAddrState", "Co Borrower Addr State"                                                   },
                { "CoBorrowerAddrZipCode", "Co Borrower Addr Zip Code"                                             },
                { "CoBorrowerPhone1", "Co Borrower Phone 1"                                                         },
                { "CoBorrowerPhone2", "Co Borrower Phone 2"                                                          },
                { "CoBorrowerPhone3", "Co Borrower Phone 3"                                                          },
                { "CoBorrowerEmailAddress", "Co Borrower Email Address"                                             },
                { "ItemFinanced", "Item financed"                                                                   },
                { "LoanAmount", "Loan Amount"                                                                       },
                { "OriginationDate", "Origination Date"                                                             },
                { "FirstPaymentDate", "First Payment Date"                                                           },
                { "ContractTermInYears", "Contract Term (Years)"                                                   },
                { "MonthlyPaymentAmount", "Monthly Payment Amount"                                                  },
                { "InterestRate", "Interest Rate"                                                                    },
                { "InterestFreeMethod", "Interest Free Method"                                                      },
                { "InterestFreePeriod", "Interest Free Period"                                                       },
                { "InterestFreeNoOfPeriods", "Interest Free # of Periods"                                            },
                { "InterestFreeMaxDpdInGracePeriod", "Interest Free Max DPD (grace period)"                          },
                { "InterestFreePayoffGrace", "Interest Free Payoff Grace"                                            },
                { "PaymentFrequency", "Payment Frequency"                                                            },
                { "GracePeriod", "Grace period"                                                                     },
                { "LateFee", "Late Fee"                                                                            },
                { "NsfFee", "NSF Fee"                                                                               },
                { "RoutingNumber", "Routing #"                                                                      },
                { "AccountNumber", "Account #"                                                                      },
                { "AccountType", "checking or savings"                                                               },
                { "MlaEligible", "MLAFLAG"                                                                           },
                { "Mapr", "MAPR" }
        };

        [Fact]
        public void OnSchedule_Test()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration() { FundedStatusCode = new string[] { "400.03" }, InServiceStatus = new InServiceStatus { Code = "600.01" }, NumberOfDays = 3, CreditPackageFileName = "LCUSA-{applicationId}-LD", CsvProjection = CsvProjection, FileStorage = new FileStorage { Host = "67.205.166.25", Port = 22, Username = "root", Password = "lendfoundry", PathOnboardFiles = "/lendfoundry" } });
            var filterView = new FilterView { Id = "1", ApplicationId = "LCUSA-0000024", SourceId = "Merchant1", SourceType = "merchant", Payout = 200, ActiveOn = new Foundation.Date.TimeBucket(DateTimeOffset.Now.AddDays(-3)) };
            ApplicationFilterService.Setup(x => x.GetByStatus(new[] { "400.03" })).Returns(async () => await Task.FromResult(new List<FilterView>() { filterView }));
            MerchantCacheService.Setup(x => x.Get("Merchant1")).Returns(new Merchant.Merchant { Id = "1", MerchantId = "Merchant1", BankAccounts = new List<Merchant.IBankAccount> { new Merchant.BankAccount { AccountNumber = "A", AccountType = Merchant.AccountType.Checking, RoutingNumber = "A", AccountHolderName = "AA", IsPrimary = true } } });
            ApplicationService.Setup(x => x.GetByApplicationNumber("LCUSA-0000024")).Returns(new Application.ApplicationResponse { ApplicationNumber = "LCUSA-0000024", Applicants = new List<Application.IApplicantRequest> { new Application.ApplicantRequest { FirstName = "A", LastName = "B", Email = "A@B.com", Id = "applicantId", DateOfBirth = DateTime.Now, IsPrimary = true } }, BankAccounts = new List<Application.IBankAccount> { new Application.BankAccount { AccountNumber = "A", AccountType = Application.BankAccountType.Savings, RoutingNumber = "A", AutoPayment = true, IsPrimary = true } } });

            StatusManagementService.Setup(x => x.ChangeStatus("application", "LCUSA-0000024", "600.01", ""));
            EventHubClient.Setup(x => x.Publish(new ApplicationOnBoardingFileCreated { Application = filterView }));
            IDictionary<string, object> dataAttributes = new Dictionary<string, object> {
                { "ApprovedAmount", 100.05  },
                { "OriginationDate", "2016-10-21"  },
                { "FirstPaymentDueDate", "2016-10-21"  },
                { "Term", 8  },
                { "InstallmentAmount", 15  },
                { "RateAmount", 15  },
                { "IsMla", "N"  }
            };
            DataAttributesEngine.Setup(x => x.GetAllAttributes("application", "LCUSA-0000024")).Returns(async () => await Task.FromResult(
               dataAttributes
               )
                );
            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            //TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            EventHubFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            StatusManagementServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationFilterServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationServiceClientFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            MerchantCacheServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            DocumentManagerServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            DataAttributesClientFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            FileStorageFactory.Verify(x => x.Create(It.IsAny<FileStorage>(), Logger.Object), Times.AtLeastOnce);
            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public void OnSchedule_When_HasNoFound_Configuration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationFactory.Setup(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()));

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            EventHubFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            StatusManagementServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationFilterServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationServiceClientFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            MerchantCacheServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            DocumentManagerServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void OnSchedule_When_HasNoFound_NumberOfDaysConfiguration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration() { FundedStatusCode = new string[] { "400.03" }, InServiceStatus = new InServiceStatus { Code = "600.01" }, CreditPackageFileName = "LCUSA-{applicationId}-LD", CsvProjection = CsvProjection, FileStorage = new FileStorage { Host = "67.205.166.25", Port = 22, Username = "root", Password = "lendfoundry", PathOnboardFiles = "/lendfoundry" } });

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            EventHubFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            StatusManagementServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationFilterServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationServiceClientFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            MerchantCacheServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            DocumentManagerServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void OnSchedule_When_HasNoFound_FundedStatusCodeConfiguration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration() { InServiceStatus = new InServiceStatus { Code = "600.01" }, NumberOfDays = 3, CreditPackageFileName = "LCUSA-{applicationId}-LD", CsvProjection = CsvProjection, FileStorage = new FileStorage { Host = "67.205.166.25", Port = 22, Username = "root", Password = "lendfoundry", PathOnboardFiles = "/lendfoundry" } });

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            EventHubFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            StatusManagementServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationFilterServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationServiceClientFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            MerchantCacheServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            DocumentManagerServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void OnSchedule_When_HasEmpty_FundedStatusCodeConfiguration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration() { FundedStatusCode = new string[] { }, InServiceStatus = new InServiceStatus { Code = "600.01" }, NumberOfDays = 3, CreditPackageFileName = "LCUSA-{applicationId}-LD", CsvProjection = CsvProjection, FileStorage = new FileStorage { Host = "67.205.166.25", Port = 22, Username = "root", Password = "lendfoundry", PathOnboardFiles = "/lendfoundry" } });

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            EventHubFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            StatusManagementServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationFilterServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationServiceClientFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            MerchantCacheServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            DocumentManagerServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void OnSchedule_When_HasNotFound_InServiceStatusCodeConfiguration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration() { FundedStatusCode = new string[] { "400.03" }, NumberOfDays = 3, CreditPackageFileName = "LCUSA-{applicationId}-LD", CsvProjection = CsvProjection, FileStorage = new FileStorage { Host = "67.205.166.25", Port = 22, Username = "root", Password = "lendfoundry", PathOnboardFiles = "/lendfoundry" } });

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            EventHubFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            StatusManagementServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationFilterServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationServiceClientFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            MerchantCacheServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            DocumentManagerServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void OnSchedule_When_HasNotFound_CreditPackageFileNameConfiguration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration() { FundedStatusCode = new string[] { "400.03" }, InServiceStatus = new InServiceStatus { Code = "600.01" }, NumberOfDays = 3, CsvProjection = CsvProjection, FileStorage = new FileStorage { Host = "67.205.166.25", Port = 22, Username = "root", Password = "lendfoundry", PathOnboardFiles = "/lendfoundry" } });

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            EventHubFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            StatusManagementServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationFilterServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationServiceClientFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            MerchantCacheServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            DocumentManagerServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }


        [Fact]
        public void OnSchedule_When_HasNoFound_CsvProjectionConfiguration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration() { FundedStatusCode = new string[] { "400.03" }, InServiceStatus = new InServiceStatus { Code = "600.01" }, NumberOfDays = 3, CreditPackageFileName = "LCUSA-{applicationId}-LD", FileStorage = new FileStorage { Host = "67.205.166.25", Port = 22, Username = "root", Password = "lendfoundry", PathOnboardFiles = "/lendfoundry" } });

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            EventHubFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            StatusManagementServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationFilterServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationServiceClientFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            MerchantCacheServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            DocumentManagerServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void OnSchedule_When_HasNoFound_EmptyCsvProjectionConfiguration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration() { FundedStatusCode = new string[] { "400.03" }, InServiceStatus = new InServiceStatus { Code = "600.01" }, NumberOfDays = 3, CreditPackageFileName = "LCUSA-{applicationId}-LD", CsvProjection = new Dictionary<string, string> { }, FileStorage = new FileStorage { Host = "67.205.166.25", Port = 22, Username = "root", Password = "lendfoundry", PathOnboardFiles = "/lendfoundry" } });

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));


            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            EventHubFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            StatusManagementServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationFilterServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationServiceClientFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            MerchantCacheServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            DocumentManagerServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void OnSchedule_When_HasNotFound_Application()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration() { FundedStatusCode = new string[] { "400.03" }, InServiceStatus = new InServiceStatus { Code = "600.01" }, NumberOfDays = 3, CreditPackageFileName = "LCUSA-{applicationId}-LD", CsvProjection = CsvProjection, FileStorage = new FileStorage { Host = "67.205.166.25", Port = 22, Username = "root", Password = "lendfoundry", PathOnboardFiles = "/lendfoundry" } });

            ApplicationFilterService.Setup(x => x.GetByStatus(new string[] { "400.03" })).Returns(async () => await Task.FromResult<IEnumerable<IFilterView>>(null));
            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));


            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            EventHubFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            StatusManagementServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationFilterServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationServiceClientFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            MerchantCacheServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            DocumentManagerServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            ApplicationFilterService.Verify(x => x.GetByStatus(new string[] { "400.03" }), Times.AtLeastOnce);
            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public void OnSchedule_When_HasNoFound_FileStorageConfiguration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration() { FundedStatusCode = new string[] { "400.03" }, InServiceStatus = new InServiceStatus { Code = "600.01" }, NumberOfDays = 3, CreditPackageFileName = "LCUSA-{applicationId}-LD", CsvProjection = CsvProjection });

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            EventHubFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            StatusManagementServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationFilterServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationServiceClientFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            MerchantCacheServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            DocumentManagerServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void OnSchedule_When_HasEmpty_FileStorageConfiguration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration() { FundedStatusCode = new string[] { "400.03" }, InServiceStatus = new InServiceStatus { Code = "600.01" }, NumberOfDays = 3, CreditPackageFileName = "LCUSA-{applicationId}-LD", CsvProjection = CsvProjection, FileStorage = new FileStorage { } });

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));


            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            EventHubFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            StatusManagementServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationFilterServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationServiceClientFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            MerchantCacheServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            DocumentManagerServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void OnSchedule_When_HasNoFound_FileStorageHostConfiguration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration() { FundedStatusCode = new string[] { "400.03" }, InServiceStatus = new InServiceStatus { Code = "600.01" }, NumberOfDays = 3, CreditPackageFileName = "LCUSA-{applicationId}-LD", CsvProjection = CsvProjection, FileStorage = new FileStorage { Port = 22, Username = "root", Password = "lendfoundry", PathOnboardFiles = "/lendfoundry" } });

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));


            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            EventHubFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            StatusManagementServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationFilterServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationServiceClientFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            MerchantCacheServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            DocumentManagerServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }



        [Fact]
        public void OnSchedule_When_HasNoFound_FileStoragePortConfiguration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration() { FundedStatusCode = new string[] { "400.03" }, InServiceStatus = new InServiceStatus { Code = "600.01" }, NumberOfDays = 3, CreditPackageFileName = "LCUSA-{applicationId}-LD", CsvProjection = CsvProjection, FileStorage = new FileStorage { Host = "67.205.166.25", Username = "root", Password = "lendfoundry", PathOnboardFiles = "/lendfoundry" } });

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            EventHubFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            StatusManagementServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationFilterServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationServiceClientFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            MerchantCacheServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            DocumentManagerServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void OnSchedule_When_HasNoFound_FileStorageUsernameConfiguration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration() { FundedStatusCode = new string[] { "400.03" }, InServiceStatus = new InServiceStatus { Code = "600.01" }, NumberOfDays = 3, CreditPackageFileName = "LCUSA-{applicationId}-LD", CsvProjection = CsvProjection, FileStorage = new FileStorage { Host = "67.205.166.25", Port = 22, Password = "lendfoundry", PathOnboardFiles = "/lendfoundry" } });

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            EventHubFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            StatusManagementServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationFilterServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationServiceClientFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            MerchantCacheServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            DocumentManagerServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void OnSchedule_When_HasNoFound_FileStoragePasswordConfiguration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration() { FundedStatusCode = new string[] { "400.03" }, InServiceStatus = new InServiceStatus { Code = "600.01" }, NumberOfDays = 3, CreditPackageFileName = "LCUSA-{applicationId}-LD", CsvProjection = CsvProjection, FileStorage = new FileStorage { Host = "67.205.166.25", Port = 22, Username = "root", PathOnboardFiles = "/lendfoundry" } });


            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            EventHubFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            StatusManagementServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationFilterServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationServiceClientFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            MerchantCacheServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            DocumentManagerServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void OnSchedule_When_HasNoFound_FileStoragePathConfiguration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration() { FundedStatusCode = new string[] { "400.03" }, InServiceStatus = new InServiceStatus { Code = "600.01" }, NumberOfDays = 3, CreditPackageFileName = "LCUSA-{applicationId}-LD", CsvProjection = CsvProjection, FileStorage = new FileStorage { Host = "67.205.166.25", Port = 22, Username = "root", Password = "lendfoundry" } });

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));


            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            EventHubFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            StatusManagementServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationFilterServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ApplicationServiceClientFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            MerchantCacheServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            DocumentManagerServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void Program_Test()
        {
            var program = new Program();

            Assert.NotNull(program);
        }

        [Fact(Skip = "For tests")]
        public void GenerateTimespan()
        {
            var pastDate = new TimeBucket(DateTimeOffset.Now.AddDays(-3));
            Console.WriteLine(pastDate);

        }
    }
}