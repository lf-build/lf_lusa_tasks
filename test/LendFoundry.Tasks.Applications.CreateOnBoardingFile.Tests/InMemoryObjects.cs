﻿using LendFoundry.Application.Client;
using LendFoundry.Applications.Filters;
using LendFoundry.Applications.Filters.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Tasks.Applications.CreateOnBoardingFile;
using LendFoundry.DocumentManager;
using LendFoundry.DataAttributes;
using LendFoundry.DataAttributes.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement;
using LendFoundry.StatusManagement.Client;
using Moq;
using LendFoundry.EventHub.Client;
using LendFoundry.Application;
using LendFoundry.Merchant;
using LendFoundry.Merchant.Client;
using LendFoundry.DocumentManager.Client;
using System;
using System.Collections.Generic;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;
using System.Text;
using System.Threading.Tasks;
using LendFoundry.Clients.BoxProxy;
using System.Linq;
namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile.Tests
{
    public class FakeFileStorageService : IFileStorageService
    {
        public void CreateDirectory(string path)
        {
            throw new NotImplementedException();
        }

        public void Delete(string fileName)
        {
            throw new NotImplementedException();
        }

        public List<string> DownloadFiles(string sourcePath)
        {
            throw new NotImplementedException();
        }

        public bool Exist(string path)
        {
            throw new NotImplementedException();
        }

        public bool IsExist(string fileName)
        {
            throw new NotImplementedException();
        }

        public void Upload(string fileFullName)
        {
            throw new NotImplementedException();
        }

        public void Upload(Stream stream, string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                throw new ArgumentException($"{nameof(fileName)} is mandatory");

        }

        public void Upload(Stream stream, string fileName, string path)
        {
            throw new NotImplementedException();
        }
    }
    public class FakeCsvGenerator : ICsvGenerator
    {
        private Dictionary<string, string> CsvProjection { get; set; }
        private CsvConfiguration SetCsvProjectionValues()
        {
            var csvConfiguration = new CsvConfiguration()
            {
                Encoding = Encoding.UTF8,

            };

            if (CsvProjection != null && CsvProjection.Any())
                csvConfiguration.RegisterClassMap(SetCsvClassMap());
            return csvConfiguration;
        }
        private CsvClassMap SetCsvClassMap()
        {
            var boardingFileMap = new DefaultCsvClassMap<BoardingFile>();
            CsvClassMap BoardingFileMap;
            Dictionary<string, string> csvProjection = CsvProjection;
            var columnIndex = 0;
            foreach (var csvKeyColumn in csvProjection)
            {
                var schemaPropertyName = csvKeyColumn.Key;
                var csvColumnName = csvKeyColumn.Value;

                if (!String.IsNullOrEmpty(csvColumnName))
                {
                    var propertyInfo = typeof(BoardingFile).GetProperty(schemaPropertyName);
                    var newMap = new CsvPropertyMap(propertyInfo);
                    newMap.Name(csvColumnName);
                    newMap.Index(columnIndex);
                    boardingFileMap.PropertyMaps.Add(newMap);
                    columnIndex += 1;
                }

            }

            BoardingFileMap = boardingFileMap;
            return BoardingFileMap;
        }
        public Byte[] Write<T>(IEnumerable<T> items, Dictionary<string, string> csvProjection)
        {
            var memoryStream = new MemoryStream();
            var streamWriter = new StreamWriter(memoryStream);
            CsvProjection = csvProjection;
            var csvWriter = new CsvWriter(streamWriter, SetCsvProjectionValues());
                  csvWriter.WriteRecords(items);
             
                return memoryStream.ToArray();
            
        }
      
      
       
    }
    public class FakeTenantTime : ITenantTime
    {
        public DateTimeOffset Now
        {
            get
            {
                return new DateTimeOffset(DateTime.Now);
            }
        }

        public TimeZoneInfo TimeZone
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public DateTimeOffset Today
        {
            get
            {
                return Now;
            }
        }

        public DateTimeOffset Convert(DateTimeOffset dateTime)
        {
            throw new NotImplementedException();
        }

        public DateTimeOffset Create(int year, int month, int day)
        {
            throw new NotImplementedException();
        }

        public DateTimeOffset FromDate(DateTime dateTime)
        {
            throw new NotImplementedException();
        }
    }

    public class FakeDocumentManagerService:IDocumentManagerService
    {
       

        public Task<IDocument> Create(Stream stream, string entity, string entityId, string fileName, List<string> tags = null)
        {
            throw new NotImplementedException();
        }

        public Task<IDocument> Create(Stream stream, string entity, string entityId, string fileName, object metadata, List<string> tags = null)
        {
            throw new NotImplementedException();
        }

        public Task Delete(string entity, string entityId, string id)
        {
            throw new NotImplementedException();
        }

        public Task<Stream> Download(string entity, string entityId, string id)
        {
            Stream s = new MemoryStream(System.Text.Encoding.ASCII.GetBytes("This is test"));
            return  Task.FromResult(s);


        }

        public Task<IDocument> Get(string entity, string entityId, string id)
        {
            return Task.FromResult<IDocument>( new Document { FileName = "A.txt", Metadata = "A", Tags = new List<string> { "A","B" }   });
        }

        public Task<IEnumerable<IDocument>> GetAll(string entity, string entityId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IDocument>> GetAllLatest(string entityType, string entityId)
        {
            return Task.FromResult<IEnumerable<IDocument>>(new List<IDocument> { new Document { FileName = "LCUSA-LCUSA-0000024-LD.txt", Metadata = "A", Tags = new List<string> { "A", "B" } }});
        }

        public Task<IDocument> Replace(Stream stream, string entity, string entityId, string id, string fileName, List<string> tags = null)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IDocument>> Search(string entity, string entityId, List<string> tags, string term)
        {
            throw new NotImplementedException();
        }

        public Task<IDocument> SetMetadata(string entity, string entityId, string id, object metadata)
        {
            throw new NotImplementedException();
        }

        public Task<IDocument> SetTags(string entity, string entityId, string id, List<string> tags)
        {
            throw new NotImplementedException();
        }
    }
    public class InMemoryObjects
    {
        public string TenantId { get; } = "my-tenant";

        // ------------------------- Configuration Objects
        protected Mock<IConfigurationServiceFactory> ConfigurationFactory { get; } = new Mock<IConfigurationServiceFactory>();
        protected Mock<IConfigurationService<Configuration>> ConfigurationService { get; } = new Mock<IConfigurationService<Configuration>>();

        // ------------------------- TenantTime Objects
        protected FakeTenantTime TenantTime { get; } = new FakeTenantTime();
        protected Mock<ITenantTimeFactory> TenantTimeFactory { get; } = new Mock<ITenantTimeFactory>();
        protected Mock<ITokenHandler> TokenHandler { get; } = new Mock<ITokenHandler>();
        // ------------------------- Eventhub Objects
        protected Mock<IEventHubClientFactory> EventHubFactory { get; } = new Mock<IEventHubClientFactory>();

        protected Mock<IEventHubClient> EventHubClient { get; } = new Mock<IEventHubClient>();

        // ------------------------- Logger Objects
        protected Mock<ILogger> Logger { get; } = new Mock<ILogger>();
        protected Mock<ILoggerFactory> LoggerFactory { get; } = new Mock<ILoggerFactory>();

        // ------------------------- CSV Generator Objects
        protected FakeCsvGenerator CsvGenerator { get; } = new FakeCsvGenerator();
        // ------------------------- Status Management Objects
        protected Mock<IEntityStatusService> StatusManagementService { get; } = new Mock<IEntityStatusService>();
        protected Mock<IStatusManagementServiceFactory> StatusManagementServiceFactory { get; } = new Mock<IStatusManagementServiceFactory>();


        // ------------------------- ApplicationFilter Objects
        protected Mock<IApplicationFilterService> ApplicationFilterService { get; } = new Mock<IApplicationFilterService>();
        protected Mock<IApplicationFilterClientFactory> ApplicationFilterServiceFactory { get; } = new Mock<IApplicationFilterClientFactory>();

        // ------------------------- Application Objects
        protected Mock<IApplicationService> ApplicationService { get; } = new Mock<IApplicationService>();
        protected Mock<IApplicationServiceClientFactory> ApplicationServiceClientFactory { get; } = new Mock<IApplicationServiceClientFactory>();

        

        // ------------------------- Merchant Objects
        protected Mock<IMerchantCacheService> MerchantCacheService { get; } = new Mock<IMerchantCacheService>();
        protected Mock<IMerchantCacheServiceFactory> MerchantCacheServiceFactory { get; } = new Mock<IMerchantCacheServiceFactory>();

        // ------------------------- DocumentManager Objects
        protected FakeDocumentManagerService DocumentManagerService { get; } = new FakeDocumentManagerService();
        protected Mock<IDocumentManagerServiceFactory> DocumentManagerServiceFactory { get; } = new Mock<IDocumentManagerServiceFactory>();

        // ------------------------- Data attribute Objects
        protected Mock<IDataAttributesEngine> DataAttributesEngine { get; } = new Mock<IDataAttributesEngine>();
        protected Mock<IDataAttributesClientFactory> DataAttributesClientFactory { get; } = new Mock<IDataAttributesClientFactory>();
        // ------------------------- File Storage Objects
        protected FakeFileStorageService FileStorageService { get; } = new FakeFileStorageService();
        protected Mock<IFileStorageFactory> FileStorageFactory { get; } = new Mock<IFileStorageFactory>();


    }
}
