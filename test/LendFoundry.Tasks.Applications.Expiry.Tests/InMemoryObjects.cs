﻿using LendFoundry.Applications.Filters;
using LendFoundry.Applications.Filters.Client;
using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement;
using LendFoundry.StatusManagement.Client;
using Moq;

namespace LendFoundry.Tasks.Applications.Expiry.Tests
{
    public abstract class InMemoryObjects
    {
        public string TenantId { get; } = "my-tenant";

        // ------------------------- Configuration Objects
        protected Mock<IConfigurationServiceFactory> ConfigurationFactory { get; } = new Mock<IConfigurationServiceFactory>();

        protected Mock<IConfigurationService<Applications.Expiry.Configuration>> ConfigurationService { get; } = new Mock<IConfigurationService<Applications.Expiry.Configuration>>();

        // ------------------------- TenantTime Objects
        protected Mock<ITenantTime> TenantTime { get; } = new Mock<ITenantTime>();

        protected Mock<ITenantTimeFactory> TenantTimeFactory { get; } = new Mock<ITenantTimeFactory>();

        protected Mock<ITokenHandler> TokenHandler { get; } = new Mock<ITokenHandler>();

        // ------------------------- EventHub Objects
        protected Mock<IEventHubClient> EventHubClient { get; } = new Mock<IEventHubClient>();

        protected Mock<IEventHubClientFactory> EventHubClientFactory { get; } = new Mock<IEventHubClientFactory>();

        // ------------------------- Status Management Objects
        protected Mock<IEntityStatusService> StatusManagementService { get; } = new Mock<IEntityStatusService>();

        protected Mock<IStatusManagementServiceFactory> StatusManagementServiceFactory { get; } = new Mock<IStatusManagementServiceFactory>();

        // ------------------------- Calendar Objects
        protected Mock<ICalendarService> CalendarService { get; } = new Mock<ICalendarService>();

        protected Mock<ICalendarServiceFactory> CalendarServiceFactory { get; } = new Mock<ICalendarServiceFactory>();

        // ------------------------- ApplicationFilter Objects
        protected Mock<IApplicationFilterService> ApplicationFilterService { get; } = new Mock<IApplicationFilterService>();

        protected Mock<IApplicationFilterClientFactory> ApplicationFilterServiceFactory { get; } = new Mock<IApplicationFilterClientFactory>();

        // ------------------------- Logger Objects
        protected Mock<ILogger> Logger { get; } = new Mock<ILogger>();

        protected Mock<ILoggerFactory> LoggerFactory { get; } = new Mock<ILoggerFactory>();
    }
}