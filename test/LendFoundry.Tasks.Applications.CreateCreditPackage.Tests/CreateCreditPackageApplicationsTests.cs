﻿
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement;
using Moq;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Microsoft.Framework.DependencyInjection;
using System;

namespace LendFoundry.Tasks.Applications.CreateCreditPackage.Tests
{
    public class CreateCreditPackageApplicationsTests : InMemoryObjects
    {
        public CreateCreditPackageApplicationsTests()
        {
            ConfigurationFactory.Setup(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()))
                .Returns(ConfigurationService.Object);

            TokenHandler.Setup(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Mock.Of<IToken>());

            TenantTimeFactory.Setup(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()))
                .Returns(TenantTime.Object);

            TokenReaderFactory.Setup(x => x.Create(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(TokenReader.Object);
           
            ApplicationServiceClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
              .Returns(ApplicationService.Object);

            DocumentGeneratorServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(DocumentGeneratorService);

            DocumentManagerServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(DocumentManagerService.Object);

            EventHubClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
               .Returns(EventHubClient.Object);

            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Info(It.IsAny<string>()));

            LoggerFactory.Setup(x => x.CreateLogger())
                .Returns(Logger.Object);

            LoggerFactory.Setup(x => x.Create(It.IsAny<ILogContext>()))
                .Returns(Logger.Object);
        }

        private LendFoundry.Tasks.Applications.CreateCreditPackage.Agent Agent => 
            new LendFoundry.Tasks.Applications.CreateCreditPackage.Agent
         (
             ConfigurationFactory.Object,
             TokenHandler.Object,
             LoggerFactory.Object,
             DocumentGeneratorServiceFactory.Object,
             ApplicationServiceClientFactory.Object,
             EventHubClientFactory.Object,
             DocumentManagerServiceFactory.Object,
             PdfMergerService.Object,
             TokenReaderFactory.Object
         );

        private Mock<ISettingReader> SettingReader { get; } = new Mock<ISettingReader>();

       
        [Fact]
        public void Agent_OnExecute_HasNullEvent()
        {
            StatusChanged statusChanged = null;
            var @event = new EventInfo { Id="Event1", Data = statusChanged, TenantId = "my-tenant" };

           
            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void Agent_OnExecute_HasNullEntityType()
        {
            var statusChanged = new StatusChanged { EntityId= "LCUSA-0000002",NewStatus="400.03",NewStatusName="Funded", OldStatus = "400.02", OldStatusName = "Funding Approved",ActiveOn = new LendFoundry.Foundation.Date.TimeBucket(System.DateTimeOffset.Now) };
            var @event = new EventInfo { Id = "Event1", Data = statusChanged, TenantId = "my-tenant" };


            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);

            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void Agent_OnExecute_HasNullEntityId()
        {
            var statusChanged = new StatusChanged { EntityType = "application", NewStatus = "400.03", NewStatusName = "Funded", OldStatus = "400.02", OldStatusName = "Funding Approved", ActiveOn = new LendFoundry.Foundation.Date.TimeBucket(System.DateTimeOffset.Now) };
            var @event = new EventInfo { Id = "Event1", Data = statusChanged, TenantId = "my-tenant" };


            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);

            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void Agent_OnExecute_HasNullConfiguration()
        {
            var @event = new EventInfo { Data = new StatusChanged { EntityType = "application", EntityId = "LCUSA-0000002", ActiveOn=new Foundation.Date.TimeBucket(System.DateTimeOffset.Now),NewStatus="400.03",NewStatusName = "Funded", OldStatus="400.02",OldStatusName="Funding Approved"  }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get());
            
            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            //TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
            Logger.Verify(x => x.Error (It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void Agent_OnExecute_HasNullFilesInConfiguration()
        {
            var @event = new EventInfo { Data = new StatusChanged { EntityType = "application", EntityId = "LCUSA-0000002", ActiveOn = new Foundation.Date.TimeBucket(System.DateTimeOffset.Now), NewStatus = "400.03", NewStatusName = "Funded", OldStatus = "400.02", OldStatusName = "Funding Approved" }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration
            {
                CertificateOfCompletionTemplate = new CertificateOfCompletionTemplate
                {
                    Name = "CertificateOfCompletionTemplate",
                    Version = "v1"
                },
                CreditPackageFileName = "LCUSA-{ApplicationNumber}-LD.pdf",
                
                FundedStatusCode = "400.02"
            });

            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            //TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void Agent_OnExecute_HasEmptyFilesInConfiguration()
        {
            var @event = new EventInfo {
                Id = Guid.NewGuid().ToString("N"),
                Data = new StatusChanged {
                    EntityType = "application",
                    EntityId = "LCUSA-0000002",
                    ActiveOn = new Foundation.Date.TimeBucket(System.DateTimeOffset.Now),
                    NewStatus = "400.03",
                    NewStatusName = "Funded",
                    OldStatus = "400.02",
                    OldStatusName = "Funding Approved" },
                TenantId = "my-tenant"
            };

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration
            {
                CertificateOfCompletionTemplate = new CertificateOfCompletionTemplate
                {
                    Name = "CertificateOfCompletionTemplate",
                    Version = "v1"
                },
                CreditPackageFileName = "LCUSA-{ApplicationNumber}-LD.pdf",
                Files = new Dictionary<string, bool> { },
                FundedStatusCode = "400.02"
            });

            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            //TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void Agent_OnExecute_HasNullFundedStatusCodeInConfiguration()
        {
            var @event = new EventInfo { Data = new StatusChanged { EntityType = "application", EntityId = "LCUSA-0000002", ActiveOn = new Foundation.Date.TimeBucket(System.DateTimeOffset.Now), NewStatus = "400.03", NewStatusName = "Funded", OldStatus = "400.02", OldStatusName = "Funding Approved" }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration
            {
                CertificateOfCompletionTemplate = new CertificateOfCompletionTemplate
                {
                    Name = "CertificateOfCompletionTemplate",
                    Version = "v1"
                },
                CreditPackageFileName = "LCUSA-{ApplicationNumber}-LD.pdf",
                Files = new Dictionary<string, bool> { { "test.pdf", true }, { "test1.pdf", true }, { "test{ApplicationNumber}.pdf", false } }               
            });

            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            //TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void Agent_OnExecute_HasNullCertificateOfCompletionTemplateInConfiguration()
        {
            var @event = new EventInfo { Data = new StatusChanged { EntityType = "application", EntityId = "LCUSA-0000002", ActiveOn = new Foundation.Date.TimeBucket(System.DateTimeOffset.Now), NewStatus = "400.03", NewStatusName = "Funded", OldStatus = "400.02", OldStatusName = "Funding Approved" }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration
            {
               
                CreditPackageFileName = "LCUSA-{ApplicationNumber}-LD.pdf",
                Files = new Dictionary<string, bool> { { "test.pdf", true }, { "test1.pdf", true }, { "test{ApplicationNumber}.pdf", false } },
                FundedStatusCode = "400.02"
            });

            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            //TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void Agent_OnExecute_HasNullCertificateOfCompletionTemplateNameInConfiguration()
        {
            var @event = new EventInfo { Data = new StatusChanged { EntityType = "application", EntityId = "LCUSA-0000002", ActiveOn = new Foundation.Date.TimeBucket(System.DateTimeOffset.Now), NewStatus = "400.03", NewStatusName = "Funded", OldStatus = "400.02", OldStatusName = "Funding Approved" }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration
            {
                CertificateOfCompletionTemplate = new CertificateOfCompletionTemplate
                {
                    Name = "",
                    Version = "v1"
                },
                CreditPackageFileName = "LCUSA-{ApplicationNumber}-LD.pdf",
                Files = new Dictionary<string, bool> { { "test.pdf", true }, { "test1.pdf", true }, { "test{ApplicationNumber}.pdf", false } },
                FundedStatusCode = "400.02"
            });

            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            //TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void Agent_OnExecute_HasNullCertificateOfCompletionTemplateVersionInConfiguration()
        {
            var @event = new EventInfo { Data = new StatusChanged { EntityType = "application", EntityId = "LCUSA-0000002", ActiveOn = new Foundation.Date.TimeBucket(System.DateTimeOffset.Now), NewStatus = "400.03", NewStatusName = "Funded", OldStatus = "400.02", OldStatusName = "Funding Approved" }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration
            {
                CertificateOfCompletionTemplate = new CertificateOfCompletionTemplate
                {
                    Name = "CertificateOfCompletionTemplate",
                    Version = ""
                },
                CreditPackageFileName = "LCUSA-{ApplicationNumber}-LD.pdf",
                Files = new Dictionary<string, bool> { { "test.pdf", true }, { "test1.pdf", true }, { "test{ApplicationNumber}.pdf", false } },
                FundedStatusCode = "400.02"
            });

            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            //TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void Agent_OnExecute_HasNotMatchNewStatusWithFundedStatusCodeInConfiguration()
        {
            var @event = new EventInfo { Data = new StatusChanged { EntityType = "application", EntityId = "LCUSA-0000002", ActiveOn = new Foundation.Date.TimeBucket(System.DateTimeOffset.Now), NewStatus = "400.01", NewStatusName = "Funding Review", OldStatus = "400.02", OldStatusName = "Funding Approved" }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get()).Returns(new Configuration
            {
                CertificateOfCompletionTemplate = new CertificateOfCompletionTemplate
                {
                    Name = "CertificateOfCompletionTemplate",
                    Version = "v1"
                },
                CreditPackageFileName = "LCUSA-{ApplicationNumber}-LD.pdf",
                Files = new Dictionary<string, bool> { { "test.pdf", true }, { "test1.pdf", true }, { "test{ApplicationNumber}.pdf", false } },
                FundedStatusCode = "400.02"
            });

            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            //TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void Agent_OnExecute_HasApplicationNotFound()
        {
            Configuration FakeConfiguration= new Configuration
                {
                    CertificateOfCompletionTemplate = new CertificateOfCompletionTemplate
                    {
                        Name = "CertificateOfCompletionTemplate",
                        Version = "v1"
                    },
                    CreditPackageFileName = "LCUSA-{ApplicationNumber}-LD.pdf",
                    Files = new Dictionary<string, bool> { { "test.pdf", true }, { "test1.pdf", true }, { "test{ApplicationNumber}.pdf", false } },
                    FundedStatusCode = "400.03"
                };
            var @event = new EventInfo { Data = new StatusChanged { EntityType = "application", EntityId = "LCUSA-0000002", ActiveOn = new Foundation.Date.TimeBucket(System.DateTimeOffset.Now), NewStatus = "400.03", NewStatusName = "Funded", OldStatus = "400.02", OldStatusName = "Funding Approved" }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get()).Returns(FakeConfiguration);
            ApplicationService.Setup(x => x.GetByApplicationNumber("LCUSA-0000002"));
            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            //TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
            ApplicationService.Verify(x => x.GetByApplicationNumber("LCUSA-0000002"), Times.AtLeastOnce);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void Agent_OnExecute_HasDocumentsNotFound()
        {
             Configuration FakeConfiguration = new Configuration
             {
                 CertificateOfCompletionTemplate = new CertificateOfCompletionTemplate
                 {
                     Name = "CertificateOfCompletionTemplate",
                     Version = "v1"
                 },
                 CreditPackageFileName = "LCUSA-{ApplicationNumber}-LD.pdf",
                 Files = new Dictionary<string, bool> { { "test.pdf", true }, { "test1.pdf", true }, { "test{ApplicationNumber}.pdf", false } },
                 FundedStatusCode = "400.03"
             };
        var @event = new EventInfo { Data = new StatusChanged { EntityType = "application", EntityId = "LCUSA-0000002", ActiveOn = new Foundation.Date.TimeBucket(System.DateTimeOffset.Now), NewStatus = "400.03", NewStatusName = "Funded", OldStatus = "400.02", OldStatusName = "Funding Approved" }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get()).Returns(FakeConfiguration);
            ApplicationService.Setup(x => x.GetByApplicationNumber("LCUSA-0000002")).Returns(new Application.ApplicationResponse { ApplicationNumber= "LCUSA-0000002" });
            DocumentManagerService.Setup(x => x.GetAllLatest("application","LCUSA-0000002")).Returns(async () =>
               await  Task.FromResult<IEnumerable<DocumentManager.IDocument>>(null));
            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            //TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
            ApplicationService.Verify(x => x.GetByApplicationNumber("LCUSA-0000002"), Times.AtLeastOnce);
            DocumentManagerService.Verify(x => x.GetAllLatest("application", "LCUSA-0000002"), Times.AtLeastOnce);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void Agent_OnExecute_HasEmptyPackageFound()
        {
            Configuration FakeConfiguration = new Configuration
            {
                CertificateOfCompletionTemplate = new CertificateOfCompletionTemplate
                {
                    Name = "CertificateOfCompletionTemplate",
                    Version = "v1"
                },
                CreditPackageFileName = "LCUSA-{ApplicationNumber}-LD.pdf",
                Files = new Dictionary<string, bool> { { "test.pdf", false }, { "test1.pdf", false }, { "test{ApplicationNumber}.pdf", false } },
                FundedStatusCode = "400.03"
            };
            var @event = new EventInfo { Data = new StatusChanged { EntityType = "application", EntityId = "LCUSA-0000002", ActiveOn = new Foundation.Date.TimeBucket(System.DateTimeOffset.Now), NewStatus = "400.03", NewStatusName = "Funded", OldStatus = "400.02", OldStatusName = "Funding Approved" }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get()).Returns(FakeConfiguration);
            ApplicationService.Setup(x => x.GetByApplicationNumber("LCUSA-0000002")).Returns(new Application.ApplicationResponse { ApplicationNumber = "LCUSA-0000002" });
            DocumentManagerService.Setup(x => x.GetAllLatest("application", "LCUSA-0000002")).Returns(async () =>
                await Task.FromResult<IEnumerable<DocumentManager.IDocument>>(new List<DocumentManager.Document> { new DocumentManager.Document { Id="1", FileName="A.pdf"  } }));
            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            //TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
            ApplicationService.Verify(x => x.GetByApplicationNumber("LCUSA-0000002"), Times.AtLeastOnce);
            DocumentManagerService.Verify(x => x.GetAllLatest("application", "LCUSA-0000002"), Times.AtLeastOnce);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void Agent_OnExecute_HasMissingRequiredFilesNotFound()
        {
            Configuration FakeConfiguration = new Configuration
            {
                CertificateOfCompletionTemplate = new CertificateOfCompletionTemplate
                {
                    Name = "CertificateOfCompletionTemplate",
                    Version = "v1"
                },
                CreditPackageFileName = "LCUSA-{ApplicationNumber}-LD.pdf",
                Files = new Dictionary<string, bool> { { "test.pdf", true  }, { "test1.pdf", true }, { "test{ApplicationNumber}.pdf", false } },
                FundedStatusCode = "400.03"
            };
            var @event = new EventInfo { Data = new StatusChanged { EntityType = "application", EntityId = "LCUSA-0000002", ActiveOn = new Foundation.Date.TimeBucket(System.DateTimeOffset.Now), NewStatus = "400.03", NewStatusName = "Funded", OldStatus = "400.02", OldStatusName = "Funding Approved" }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get()).Returns(FakeConfiguration);
            ApplicationService.Setup(x => x.GetByApplicationNumber("LCUSA-0000002")).Returns(new Application.ApplicationResponse { ApplicationNumber = "LCUSA-0000002" });
            DocumentManagerService.Setup(x => x.GetAllLatest("application", "LCUSA-0000002")).Returns(async () =>
                await Task.FromResult<IEnumerable<DocumentManager.IDocument>>(new List<DocumentManager.Document> { new DocumentManager.Document { Id = "1", FileName = "A.pdf" } }));
          
            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            //TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
            ApplicationService.Verify(x => x.GetByApplicationNumber("LCUSA-0000002"), Times.AtLeastOnce);
            DocumentManagerService.Verify(x => x.GetAllLatest("application", "LCUSA-0000002"), Times.AtLeastOnce);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
           
        }
        [Fact]
        public void Agent_OnExecute_HasCertificateOfCompletionResponseContentNull()
        {
            Configuration FakeConfiguration = new Configuration
            {
                CertificateOfCompletionTemplate = new CertificateOfCompletionTemplate
                {
                    Name = "NullContentCertificateOfCompletionTemplate",
                    Version = "v1"
                },
                CreditPackageFileName = "LCUSA-{ApplicationNumber}-LD.pdf",
                Files = new Dictionary<string, bool> { { "test.pdf", true }, { "test1.pdf", true }, { "test{ApplicationNumber}.pdf", false } },
                FundedStatusCode = "400.03"
            };
            var @event = new EventInfo { Data = new StatusChanged { EntityType = "application", EntityId = "LCUSA-0000002", ActiveOn = new Foundation.Date.TimeBucket(System.DateTimeOffset.Now), NewStatus = "400.03", NewStatusName = "Funded", OldStatus = "400.02", OldStatusName = "Funding Approved" }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get()).Returns(FakeConfiguration);
            ApplicationService.Setup(x => x.GetByApplicationNumber("LCUSA-0000002")).Returns(new Application.ApplicationResponse { ApplicationNumber = "LCUSA-0000002",SubmittedDate=new LendFoundry.Foundation.Date.TimeBucket(System.DateTimeOffset.Now),Applicants = new List<Application.IApplicantRequest> { new Application.ApplicantRequest { Email ="A@p.com",ApplicantId = "1",FirstName="A",LastName="A",IsPrimary=true } } });
            DocumentManagerService.Setup(x => x.GetAllLatest("application", "LCUSA-0000002")).Returns(async () =>
                await Task.FromResult<IEnumerable<DocumentManager.IDocument>>(new List<DocumentManager.Document> { new DocumentManager.Document { Id = "1", FileName = "test.pdf" }, new DocumentManager.Document { Id = "2", FileName = "test1.pdf" } }));

            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            //TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
            ApplicationService.Verify(x => x.GetByApplicationNumber("LCUSA-0000002"), Times.AtLeastOnce);
            DocumentManagerService.Verify(x => x.GetAllLatest("application", "LCUSA-0000002"), Times.AtLeastOnce);
            
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void Agent_OnExecute_HasCertificateOfCompletionTemplateContentLengthZero()
        {
            Configuration FakeConfiguration = new Configuration
            {
                CertificateOfCompletionTemplate = new CertificateOfCompletionTemplate
                {
                    Name = "CertificateOfCompletionTemplateContentLengthZero",
                    Version = "v1"
                },
                CreditPackageFileName = "LCUSA-{ApplicationNumber}-LD.pdf",
                Files = new Dictionary<string, bool> { { "test.pdf", true }, { "test1.pdf", true }, { "test{ApplicationNumber}.pdf", false } },
                FundedStatusCode = "400.03"
            };
            var @event = new EventInfo { Data = new StatusChanged { EntityType = "application", EntityId = "LCUSA-0000002", ActiveOn = new Foundation.Date.TimeBucket(System.DateTimeOffset.Now), NewStatus = "400.03", NewStatusName = "Funded", OldStatus = "400.02", OldStatusName = "Funding Approved" }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get()).Returns(FakeConfiguration);
            ApplicationService.Setup(x => x.GetByApplicationNumber("LCUSA-0000002")).Returns(new Application.ApplicationResponse { ApplicationNumber = "LCUSA-0000002", SubmittedDate = new LendFoundry.Foundation.Date.TimeBucket(System.DateTimeOffset.Now), Applicants = new List<Application.IApplicantRequest> { new Application.ApplicantRequest { Email = "A@p.com", ApplicantId = "1", FirstName = "A", LastName = "A", IsPrimary = true } } });
            DocumentManagerService.Setup(x => x.GetAllLatest("application", "LCUSA-0000002")).Returns(async () =>
                await Task.FromResult<IEnumerable<DocumentManager.IDocument>>(new List<DocumentManager.Document> { new DocumentManager.Document { Id = "1", FileName = "test.pdf" }, new DocumentManager.Document { Id = "2", FileName = "test1.pdf" } }));

            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
            //TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
            ApplicationService.Verify(x => x.GetByApplicationNumber("LCUSA-0000002"), Times.AtLeastOnce);
            DocumentManagerService.Verify(x => x.GetAllLatest("application", "LCUSA-0000002"), Times.AtLeastOnce);

            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);

        }
        //[Fact]
        //public void Agent_OnExecute_HasUnableToUploadCertificate()
        //{
        //    Configuration FakeConfiguration = new Configuration
        //    {
        //        CertificateOfCompletionTemplate = new CertificateOfCompletionTemplate
        //        {
        //            Name = "CertificateOfCompletionTemplate",
        //            Version = "v1"
        //        },
        //        CreditPackageFileName = "LCUSA-{ApplicationNumber}-LD.pdf",
        //        Files = new Dictionary<string, bool> { { "test.pdf", true }, { "test1.pdf", true }, { "test{ApplicationNumber}.pdf", false } },
        //        FundedStatusCode = "400.03"
        //    };
        //    var @event = new EventInfo { Data = new StatusChanged { EntityType = "application", EntityId = "LCUSA-0000002", ActiveOn = new Foundation.Date.TimeBucket(System.DateTimeOffset.Now), NewStatus = "400.03", NewStatusName = "Funded", OldStatus = "400.02", OldStatusName = "Funding Approved" }, TenantId = "my-tenant" };

        //    ConfigurationService.Setup(x => x.Get()).Returns(FakeConfiguration);
        //    ApplicationService.Setup(x => x.GetByApplicationNumber("LCUSA-0000002")).Returns(new Application.ApplicationResponse { ApplicationNumber = "LCUSA-0000002", SubmittedDate = new LendFoundry.Foundation.Date.TimeBucket(System.DateTimeOffset.Now), Applicants = new List<Application.IApplicantRequest> { new Application.ApplicantRequest { Email = "A@p.com", ApplicantId = "1", FirstName = "A", LastName = "A", IsPrimary = true } } });
        //    DocumentManagerService.Setup(x => x.GetAll("application", "LCUSA-0000002")).Returns(async () =>
        //        await Task.FromResult<IEnumerable<DocumentManager.IDocument>>(new List<DocumentManager.Document> { new DocumentManager.Document { Id = "1", FileName = "test.pdf" }, new DocumentManager.Document { Id = "2", FileName = "test1.pdf" } }));
        //    DocumentManagerService.Setup(x => x.Download("application", "LCUSA-0000002","1")).Returns(
        //        async () => await Task.FromResult<Stream>(
        //        new System.IO.MemoryStream(System.Text.Encoding.ASCII.GetBytes("whatever"))                
        //            ));
        //    DocumentManagerService.Setup(x => x.Download("application", "LCUSA-0000002", "2")).Returns(
        //       async () => await Task.FromResult<Stream>(
        //       new System.IO.MemoryStream(System.Text.Encoding.ASCII.GetBytes("whatever"))
        //           ));
        //    Agent.Execute(@event);

        //    LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeastOnce);
        //    //TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
        //    ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
        //    ApplicationService.Verify(x => x.GetByApplicationNumber("LCUSA-0000002"), Times.AtLeastOnce);
        //    DocumentManagerService.Verify(x => x.GetAll("application", "LCUSA-0000002"), Times.AtLeastOnce);

        //    Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);

        //}
        [Fact]
        public void Program_Test()
        {
            var program = new Program();

            Assert.NotNull(program);
        }
    }
}