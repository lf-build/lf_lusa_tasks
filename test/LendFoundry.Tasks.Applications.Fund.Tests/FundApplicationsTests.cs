﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Applications.Filters;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.MoneyMovement.Ach;
using LendFoundry.Security.Tokens;
using Moq;
using Xunit;

namespace LendFoundry.Tasks.Applications.Fund.Tests
{
    public class FundApplicationsTests : InMemoryObjects
    {
        public FundApplicationsTests()
        {
            ConfigurationFactory.Setup(x => x.Create<Configuration>(It.IsAny<string >(), It.IsAny<ITokenReader>()))
                .Returns(ConfigurationService.Object);

            TokenHandler.Setup(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Mock.Of<IToken>());

            TenantTimeFactory.Setup(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()))
                .Returns(TenantTime.Object);


            //CalendarServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
            //    .Returns(CalendarService.Object);

            AchServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(AchService.Object);
            StatusManagementServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
             .Returns(StatusManagementService.Object);

            ApplicationFilterServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(ApplicationFilterService.Object);
            MerchantCacheFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(MerchantService.Object);
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            LoggerFactory.Setup(x => x.Create(It.IsAny<ILogContext>()))
                .Returns(Logger.Object);
            DataAttributesClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
 .Returns(DataAttributesEngine.Object);
        }

        private Agent Agent => new Agent
        (
            ConfigurationFactory.Object,
            TokenHandler.Object,
            TenantTimeFactory.Object,
            AchServiceFactory.Object,
            LoggerFactory.Object,
            StatusManagementServiceFactory.Object,
            ApplicationFilterServiceFactory.Object,
            //MerchantServiceFactory.Object,
            ApplicationServiceClientFactory.Object,
            MerchantCacheFactory.Object,
            EventHubClientFactory.Object,
            DataAttributesClientFactory.Object
        );

        private Mock<ISettingReader> SettingReader { get; } = new Mock<ISettingReader>();

        [Fact]
        public void OnSchedule_Test()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationService.Setup(x => x.Get())
                .Returns(new Configuration
                {
                    FundingApprovedStatusCodes = new[] {"400.02" },
                   FundedStatus = new StatusConfiguration { Code="400.03"},
                   AchConfiguration = new AchConfiguration()
                   {
                       FundingSourceId = "crb"
                   }
                });
            IInstruction instructionResponse = new Instruction
            {
                Id = "AchFileId"
            };
            AchService.Setup(x => x.Queue(new InstructionRequest()

            {
                Amount = 200,
                Frequency = Frequency.Single,
                Type = TransactionType.CreditToSavings,
                FundingSourceId = "Merchant1",
                StandardEntryClassCode = "CCD"
            }))
                .Returns(instructionResponse);
            ApplicationFilterService.Setup(x => x.GetByStatus(new[] { "400.02" })).Returns(async () => await Task.FromResult( new List<FilterView>() {new FilterView {  Id = "1", ApplicationId = "Application1", SourceId ="Merchant1",SourceType="merchant" , Payout = 200 } }));
            MerchantService.Setup(x => x.Get("Merchant1")).Returns(new Merchant.Merchant { Id = "1", MerchantId = "Merchant1", BankAccounts = new List<Merchant.IBankAccount>() { new Merchant.BankAccount { AccountHolderName = "Test", AccountNumber = "123", AccountType = Merchant.AccountType.Checking, IsPrimary = true, RoutingNumber = "123" } } });
            StatusManagementService.Setup(x => x.GetStatusByEntity(It.IsAny<string>(),It.IsAny<string>())).Returns(new StatusManagement.StatusResponse { Code="400.02" });

            StatusManagementService.Setup(x => x.ChangeStatus( "application", "Application1", "400.03",""));
            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            AchServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
            ApplicationFilterService.Verify(x => x.GetByStatus(new[] { "400.02" }), Times.AtLeastOnce);
            MerchantService.Verify(x => x.Get("Merchant1"), Times.AtLeastOnce);
            AchService.Verify(x => x.Queue(new InstructionRequest ()
            {
                Amount = 200,
                Frequency = Frequency.Single,
                Type = TransactionType.CreditToSavings,
                FundingSourceId = "Merchant1",
                StandardEntryClassCode = "CCD"
            }), Times.Never); // Need to update with Times.AtLeastOnce once queue is ready
            StatusManagementService.Verify(x => x.ChangeStatus("application", "Application1", "400.03", ""), Times.Never);// Need to update with Times.AtLeastOnce once queue is ready
        }

        [Fact]
        public void OnSchedule_When_HasNoFound_Configuration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationFactory.Setup(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()));

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            AchServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            
            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void OnSchedule_When_HasNoFound_FundApplicationConfiguration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");


            ConfigurationService.Setup(x => x.Get());

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            AchServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void OnSchedule_When_HasNoFound_FundingApprovedStatusCode()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationService.Setup(x => x.Get())
                .Returns(new Configuration
                {
                   FundedStatus=new StatusConfiguration { Code =  "400.03"}
                });

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            AchServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }
        [Fact]
        public void OnSchedule_When_HasNoFound_FundedStatusCode()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            ConfigurationService.Setup(x => x.Get())
                .Returns(new Configuration
                {
                    FundingApprovedStatusCodes =new[] { "400.02" }
                });

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            AchServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never );
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void Program_Test()
        {
            var program = new Program();

            Assert.NotNull(program);

           
        }
    }
}