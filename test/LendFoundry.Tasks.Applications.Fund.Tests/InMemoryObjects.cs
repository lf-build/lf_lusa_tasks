﻿using LendFoundry.Application.Client;
using LendFoundry.Applications.Filters;
using LendFoundry.Applications.Filters.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.DataAttributes;
using LendFoundry.DataAttributes.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.Notifications.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement;
using LendFoundry.StatusManagement.Client;
using Moq;

namespace LendFoundry.Tasks.Applications.Fund.Tests
{
    public class InMemoryObjects
    {
        public string TenantId { get; } = "my-tenant";

        // ------------------------- Configuration Objects
        protected Mock<IConfigurationServiceFactory> ConfigurationFactory { get; } = new Mock<IConfigurationServiceFactory>();
        protected Mock<IConfigurationService<Configuration>> ConfigurationService { get; } = new Mock<IConfigurationService<Configuration>>();

        // ------------------------- TenantTime Objects
        protected Mock<ITenantTime> TenantTime { get; } = new Mock<ITenantTime>();
        protected Mock<ITenantTimeFactory> TenantTimeFactory { get; } = new Mock<ITenantTimeFactory>();
        protected Mock<ITokenHandler> TokenHandler { get; } = new Mock<ITokenHandler>();

      

        // ------------------------- Calendar Objects
        //protected Mock<ICalendarService> CalendarService { get; } = new Mock<ICalendarService>();
        //protected Mock<ICalendarServiceFactory> CalendarServiceFactory { get; } = new Mock<ICalendarServiceFactory>();

        protected Mock<INotificationServiceFactory> NotificationFactory { get; } = new Mock<INotificationServiceFactory>();

        // ------------------------- Logger Objects
        protected Mock<ILogger> Logger { get; } = new Mock<ILogger>();
        protected Mock<ILoggerFactory> LoggerFactory { get; } = new Mock<ILoggerFactory>();

        // ------------------------- Ach Objects
        protected Mock<IAchService> AchService { get; } = new Mock<IAchService>();
        protected Mock<IAchServiceFactory> AchServiceFactory { get; } = new Mock<IAchServiceFactory>();

        // ------------------------- ApplicationFilter Objects
        protected Mock<IApplicationFilterService> ApplicationFilterService { get; } = new Mock<IApplicationFilterService>();
        protected Mock<IApplicationFilterClientFactory> ApplicationFilterServiceFactory { get; } = new Mock<IApplicationFilterClientFactory>();

        // ------------------------- Status Management Objects
        protected Mock<IEntityStatusService> StatusManagementService { get; } = new Mock<IEntityStatusService>();
        protected Mock<IStatusManagementServiceFactory> StatusManagementServiceFactory { get; } = new Mock<IStatusManagementServiceFactory>();

        // ------------------------- Merchant Objects
        //protected Mock<IMerchantService> MerchantService { get; } = new Mock<IMerchantService>();
        //protected Mock<IMerchantServiceFactory> MerchantServiceFactory { get; } = new Mock<IMerchantServiceFactory>();

        protected Mock<IApplicationServiceClientFactory> ApplicationServiceClientFactory { get; } = new Mock<IApplicationServiceClientFactory>();
        protected Mock<IMerchantCacheService> MerchantService { get; } = new Mock<IMerchantCacheService>();
        protected Mock<IMerchantCacheServiceFactory> MerchantCacheFactory { get; } = new Mock<IMerchantCacheServiceFactory>();

        protected Mock<IEventHubClient> EventHubClient { get; } = new Mock<IEventHubClient>();

        protected Mock<IEventHubClientFactory> EventHubClientFactory { get; } = new Mock<IEventHubClientFactory>();

        // ------------------------- Data attribute Objects
        protected Mock<IDataAttributesEngine> DataAttributesEngine { get; } = new Mock<IDataAttributesEngine>();
        protected Mock<IDataAttributesClientFactory> DataAttributesClientFactory { get; } = new Mock<IDataAttributesClientFactory>();
    }
}
