FROM registry.lendfoundry.com/base:beta8

ENV MONO_THREADS_PER_CPU 2000
ADD ./src/LendFoundry.Tasks.Abstractions /app/LendFoundry.Tasks.Abstractions
ADD ./src/LendFoundry.Tasks.Agent /app/LendFoundry.Tasks.Agent
ADD ./src/LendFoundry.Tasks.Merchants.SendEmailsOnEnrolledActive /app/LendFoundry.Tasks.Merchants.SendEmailsOnEnrolledActive

WORKDIR /app/LendFoundry.Tasks.Abstractions
RUN eval "$CMD_RESTORE"
RUN dnu build

WORKDIR /app/LendFoundry.Tasks.Agent
RUN eval "$CMD_RESTORE"
RUN dnu build

WORKDIR /app/LendFoundry.Tasks.Merchants.SendEmailsOnEnrolledActive
RUN eval "$CMD_RESTORE"
RUN dnu build

ENTRYPOINT dnx run