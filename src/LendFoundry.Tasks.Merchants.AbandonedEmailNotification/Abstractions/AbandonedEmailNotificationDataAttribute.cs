﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Tasks.Merchants.AbandonedEmailNotification.Abstractions
{
    public class AbandonedEmailNotificationDataAttribute
    {
        public DateTimeOffset SentDate { get; set; }
        public int TotalEmailSent { get; set; }
    }
}
