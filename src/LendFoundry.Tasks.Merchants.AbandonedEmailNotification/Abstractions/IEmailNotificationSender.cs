﻿namespace LendFoundry.Tasks.Merchants.AbandonedEmailNotification.Abstractions
{
    public interface IEmailNotificationSender
    {
        void Send();
    }
}