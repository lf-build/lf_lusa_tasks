﻿using LendFoundry.Configuration.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.Email.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Merchant.Client;
using LendFoundry.Merchant.Filters.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Agent;
using LendFoundry.Tasks.Merchants.AbandonedEmailNotification.Services;
using System;
using System.Linq;

namespace LendFoundry.Tasks.Merchants.AbandonedEmailNotification
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,

            ITenantTimeFactory tenantTimeFactory,
            ILoggerFactory loggerFactory,
            IMerchantServiceFactory merchantServiceFactory,
            IMerchantFilterFactory merchantFilterFactory,
            IDataAttributesClientFactory dataAttrbituesFactory,
            IEventHubClientFactory eventhubFactory,
            IEmailServiceFactory emailServiceFactory

        ) : base(tokenHandler, configurationFactory, tenantTimeFactory)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            LoggerFactory = loggerFactory;
            MerchantServiceFactory = merchantServiceFactory;
            MerchantFilterFactory = merchantFilterFactory;

            EventhubFactory = eventhubFactory;
            EmailServiceFactory = emailServiceFactory;
            DataAttrbituesFactory = dataAttrbituesFactory;
        }

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IMerchantServiceFactory MerchantServiceFactory { get; }
        private IMerchantFilterFactory MerchantFilterFactory { get; }

        private IEmailServiceFactory EmailServiceFactory { get; }
        private IDataAttributesClientFactory DataAttrbituesFactory { get; }

        private IEventHubClientFactory EventhubFactory { get; set; }

        public override void OnSchedule()

        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            try
            {
                logger.Info("OnSchedule method started");

                // gets the context user
                var token = TokenHandler.Issue(Tenant, Settings.ServiceName);
                var reader = new StaticTokenReader(token.Value);

                // task service dependencies
                var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
                if (configurationService == null)
                {
                    logger.Error($"Did not find the configuration related to {Settings.ServiceName}");
                    return;
                }
                var configuration = configurationService.Get();
                if (configuration == null)
                {
                    logger.Error($"The configuration related to {Settings.ServiceName} was not found");
                    return;
                }
                if (string.IsNullOrEmpty(configuration.TemplateName))
                {
                    logger.Error($"The configuration with template name related to {Settings.ServiceName} was not found");
                    return;
                }
                if (string.IsNullOrEmpty(configuration.TemplateVersion))
                {
                    logger.Error($"The configuration with template version related to {Settings.ServiceName} was not found");
                    return;
                }
                if (configuration.StatusNotCompleted == null || !configuration.StatusNotCompleted.Any())
                {
                    logger.Error($"The configuration with template version related to {Settings.ServiceName} was not found");
                    return;
                }
                var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);

                var merchantFilter = MerchantFilterFactory.Create(reader);
                var merchantService = MerchantServiceFactory.Create(reader);
                var eventHub = EventhubFactory.Create(reader);
                var emailService = EmailServiceFactory.Create(reader);
                var dataAttributeEngine = DataAttrbituesFactory.Create(reader);

                // start tasks process
                new EmailNotificationSender(configuration, tenantTime, merchantService, merchantFilter, eventHub, emailService, dataAttributeEngine, logger).Send();
            }
            catch (Exception ex)
            {
                logger.Error("Error while executing the OnSchedule method from AbandonedEmailNotification task. Error: ", ex);
            }
        }
    }
}