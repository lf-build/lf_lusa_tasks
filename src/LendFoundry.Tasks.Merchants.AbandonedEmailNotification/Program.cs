﻿using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.Email.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Merchant.Client;
using LendFoundry.Merchant.Filters.Client;
using LendFoundry.Security.Tokens;
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Tasks.Merchants.AbandonedEmailNotification
{
    public class Program : DependencyInjection
    {
        public void Main()
        {
            Provider.GetService<IAgent>().Execute();
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddCalendarService(Settings.Calendar.Host, Settings.Calendar.Port);
            services.AddMerchant(Settings.Merchant.Host, Settings.Merchant.Port);
            services.AddLoanFilter(Settings.MerchantFilters.Host, Settings.MerchantFilters.Port);
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddEmailService(Settings.Email.Host, Settings.Email.Port);
            services.AddDataAttributes(Settings.DataAttributes.Host, Settings.DataAttributes.Port);
            services.AddConfigurationService<Configuration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddTransient<IAgent, Agent>();

            return services;
        }
    }
}