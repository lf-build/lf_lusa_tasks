﻿using LendFoundry.DataAttributes;
using LendFoundry.Email;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Merchant;
using LendFoundry.Merchant.Filters;
using LendFoundry.Tasks.Merchants.AbandonedEmailNotification.Abstractions;
using LendFoundry.Tasks.Merchants.AbandonedEmailNotification.Events;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Tasks.Merchants.AbandonedEmailNotification.Services
{
    public class EmailNotificationSender : IEmailNotificationSender
    {
        public EmailNotificationSender
        (
            Configuration configuration,
            ITenantTime tenantTime,
            IMerchantService merchantService,
            IMerchantFilterService merchantFilterService,
            IEventHubClient eventHub,
            IEmailService emailService,
            IDataAttributesEngine dataAttributes,
            ILogger logger
        )
        {
            EnsureParameter(nameof(configuration), configuration);
            EnsureParameter(nameof(tenantTime), tenantTime);
            EnsureParameter(nameof(merchantService), merchantService);
            EnsureParameter(nameof(merchantFilterService), merchantFilterService);
            EnsureParameter(nameof(eventHub), eventHub);
            EnsureParameter(nameof(emailService), emailService);
            EnsureParameter(nameof(dataAttributes), dataAttributes);
            EnsureParameter(nameof(logger), logger);

            Configuration = configuration;
            TenantTime = tenantTime;
            MerchantService = merchantService;
            MerchantFilterService = merchantFilterService;
            DataAttributes = dataAttributes;
            EventHub = eventHub;
            EmailService = emailService;
            Logger = logger;
        }

        private Configuration Configuration { get; }
        private ITenantTime TenantTime { get; }
        private IMerchantService MerchantService { get; }
        private IMerchantFilterService MerchantFilterService { get; }
        private IDataAttributesEngine DataAttributes { get; }
        private IEventHubClient EventHub { get; }
        private IEmailService EmailService { get; }
        private ILogger Logger { get; }

        public async void Send()
        {
            Logger.Info("Searching for not completed merchants");
            var statusNotCompleted = Configuration.StatusNotCompleted;

            var merchantFilters = await MerchantFilterService.GetByStatus(statusNotCompleted);

            merchantFilters = merchantFilters.Where(
                p =>
                    p.ActiveOn != null &&
                    p.ActiveOn.Time <= TenantTime.Now.AddHours(-Configuration.NumberOfHours)
            );
           
            if(merchantFilters==null)
            {
                Logger.Info($"Did not find any merchant(s) that are not completed.");
                return;
            }
            var filterViews = merchantFilters as IList<IFilterView> ?? merchantFilters.ToList();

            if (!filterViews.Any())
            {
                Logger.Info($"Did not find any merchant(s) that are not completed {Configuration.NumberOfHours} hours ago.");
                return;
            }

            Logger.Info($"Total of {filterViews.Count()} merchants found");
            Parallel.ForEach(filterViews, merchantFilter =>
            {
                try
                {
                    var merchant = MerchantService.Get(merchantFilter.MerchantId);

                    var totalEmailSent = 0;
                    if (merchant == null)
                    {
                        Logger.Info($"Did not find merchant {merchantFilter.MerchantId}.");
                        return;
                    }

                    var dataAttributesResult = DataAttributes
                        .GetAttribute("merchant", merchantFilter.MerchantId, "abandonedEmailNotification")
                        .Result;

                    var canSendEmail = false;
                    if (dataAttributesResult != null)
                    {
                        var attributes = JObject.FromObject(dataAttributesResult).ToObject<AbandonedEmailNotificationDataAttribute>();
                        totalEmailSent = attributes.TotalEmailSent;
                        if (totalEmailSent < 3)
                        {
                            var dateDiff = TenantTime.Today.Subtract(attributes.SentDate);
                            if (dateDiff.TotalDays >= 7)
                            {
                                canSendEmail = true;
                            }
                        }
                    }
                    else
                    {
                        canSendEmail = true;
                    }

                    if (canSendEmail)
                    {
                        var merchantPrimaryContact = merchant.Contacts?.FirstOrDefault(p => p.IsPrimary);
                        if (merchantPrimaryContact == null)
                        {
                            Logger.Info($"Did not find merchant {merchantFilter.MerchantId} primary contact.");
                            return;
                        }

                        DataAttributes.SetAttribute("merchant", merchant.MerchantId,
                            "abandonedEmailNotification",
                            new AbandonedEmailNotificationDataAttribute
                            {
                                SentDate = TenantTime.Today,
                                TotalEmailSent = totalEmailSent + 1
                            }).Wait();

                        Logger.Info(
                            $"Starting to send email notification for {merchantFilter.MerchantId} with {merchantPrimaryContact.Email}");
                        var request = new
                        {
                            templateName = Configuration.TemplateName,
                            templateVersion = Configuration.TemplateVersion,
                            merchantfirstname = merchantPrimaryContact.FirstName,
                            name = merchantFilter.Contact,
                            actionlink = Settings.Link,                          
                            email = merchant.Email,
                            customeremail = merchant.Email
                        };

                        var isSent = EmailService.Send(Configuration.TemplateName, Configuration.TemplateVersion, request).Result;
                        if (isSent)
                            Logger.Info($"finished to send email notification for {merchantFilter.MerchantId}");
                    }
                    else
                    {
                        Logger.Info($"Merchant {merchantFilter.MerchantId} has already sent email in this week.");
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error($"Error while send email notification for {merchantFilter.MerchantId}", ex);
                    EventHub.Publish(new MerchantSendEmailNotifyFailed { Merchant = merchantFilter }).Wait();
                }
            });
        }

        private static void EnsureParameter(string name, object value)
        {
            if (value == null)
                throw new ArgumentNullException($"{name} cannot be null.");
        }
    }

    
}