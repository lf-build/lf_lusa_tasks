﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Tasks.Merchants.AbandonedEmailNotification
{
    public class Settings
    {
        public const string Prefix = "TASK_MERCHANTS_ABANDONED_EMAIL_NOTIFICATION";

        public static string ServiceName { get; } = Prefix.ToLower();

        public static ServiceSettings MerchantFilters { get; } = new ServiceSettings($"{Prefix}_MERCHANTFILTER_HOST", "merchant-filter", $"{Prefix}_MERCHANTFILTER_PORT");
        public static ServiceSettings Merchant { get; } = new ServiceSettings($"{Prefix}_MERCHANT_HOST", "merchant", $"{Prefix}_MERCHANT_PORT");
        public static ServiceSettings DataAttributes { get; } = new ServiceSettings($"{Prefix}_DATAATTRIBUTES_HOST", "data-attribute", $"{Prefix}_DATAATTRIBUTES_PORT");

        public static ServiceSettings Calendar { get; } = new ServiceSettings($"{Prefix}_CALENDAR_HOST", "calendar", $"{Prefix}_CALENDAR_PORT");

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB_HOST", "eventhub", $"{Prefix}_EVENTHUB_PORT");

        public static ServiceSettings Email { get; } = new ServiceSettings($"{Prefix}_EMAIL_HOST", "email", $"{Prefix}_EMAIL_PORT");

        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats://nats:4222";

        public static string Link => Environment.GetEnvironmentVariable($"{Prefix}_LINK_URL") ?? "link";
    }
}