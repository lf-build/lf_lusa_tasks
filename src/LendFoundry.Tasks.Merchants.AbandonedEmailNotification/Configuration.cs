﻿namespace LendFoundry.Tasks.Merchants.AbandonedEmailNotification
{
    public class Configuration
    {
        public string[] StatusNotCompleted { get; set; }
        public int NumberOfHours { get; set; }
        public string TemplateName { get; set; }

        public string TemplateVersion { get; set; }
       
    }
}