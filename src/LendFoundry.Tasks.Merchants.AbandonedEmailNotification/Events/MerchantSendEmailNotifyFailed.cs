﻿using LendFoundry.Merchant.Filters;

namespace LendFoundry.Tasks.Merchants.AbandonedEmailNotification.Events
{
    public class MerchantSendEmailNotifyFailed
    {
        public IFilterView Merchant { get; set; }
    }
}