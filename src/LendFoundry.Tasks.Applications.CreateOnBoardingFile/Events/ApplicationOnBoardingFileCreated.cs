﻿using LendFoundry.Merchant.Filters;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public class ApplicationOnBoardingFileCreated
    {
        public IFilterView Application { get; set; }
    }
}