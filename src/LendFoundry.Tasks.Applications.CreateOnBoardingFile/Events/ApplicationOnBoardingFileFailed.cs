﻿using LendFoundry.Merchant.Filters;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public class ApplicationOnBoardingFileFailed
    {
        public IFilterView Application { get; set; }
    }
}