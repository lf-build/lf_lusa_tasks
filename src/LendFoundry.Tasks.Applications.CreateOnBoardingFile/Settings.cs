﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public static class Settings
    {
        public const string Prefix = "TASK_APPLICATIONS_CREATE_ONBOARDING_FILE";

        public static string ServiceName { get; } = Prefix.ToLower();

        public static ServiceSettings Application { get; } = new ServiceSettings($"{Prefix}_APPLICATION_HOST", "application", $"{Prefix}_APPLICATION_PORT");

        public static ServiceSettings ApplicationFilter { get; } = new ServiceSettings($"{Prefix}_APPLICATIONFILTER_HOST", "application-filters", $"{Prefix}_APPLICATIONFILTER_PORT");

        public static ServiceSettings BoxProxy { get; } = new ServiceSettings($"{Prefix}_BOXPROXY_HOST", "box-proxy", $"{Prefix}_BOXPROXY_PORT");

        public static ServiceSettings Calendar { get; } = new ServiceSettings($"{Prefix}_CALENDAR_HOST", "calendar", $"{Prefix}_CALENDAR_PORT");

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");

        public static ServiceSettings DataAttributes { get; } = new ServiceSettings($"{Prefix}_DATAATTRIBUTE_HOST", "data-attribute", $"{Prefix}_DATAATTRIBUTE_PORT");

        public static ServiceSettings DocumentManager { get; } = new ServiceSettings($"{Prefix}_DOCUMENTMANAGER_HOST", "document-manager", $"{Prefix}_DOCUMENTMANAGER_PORT");

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB_HOST", "eventhub", $"{Prefix}_EVENTHUB_PORT");

        public static ServiceSettings Merchant { get; } = new ServiceSettings($"{Prefix}_MERCHANT_HOST", "merchant", $"{Prefix}_MERCHANT_PORT");

        public static ServiceSettings StatusManagement { get; } = new ServiceSettings($"{Prefix}_STATUSMANAGEMENT_HOST", "status-management", $"{Prefix}_STATUSMANAGEMENT_PORT");

        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats://nats:4222";
    }
}