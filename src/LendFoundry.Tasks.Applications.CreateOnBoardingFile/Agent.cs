﻿using LendFoundry.Merchant.Application.Client;
using LendFoundry.Merchant.Applications.Filters.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.DocumentManager.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement.Client;
using LendFoundry.Tasks.Agent;
using System;
using LendFoundry.Configuration;
using LendFoundry.EventHub;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ITenantTimeFactory tenantTimeFactory,
            ILoggerFactory loggerFactory,
            ICsvGenerator csvGenerator,
            IStatusManagementServiceFactory statusManagementServiceFactory,
            IApplicationFilterClientFactory applicationFilterClientFactory,
            IApplicationServiceClientFactory applicationServiceClientFactory,
            IMerchantCacheServiceFactory merchantCacheFactory,
            IDocumentManagerServiceFactory documentManagerServiceFactory,
            IDataAttributesClientFactory dataAttrbituesFactory,
             IFileStorageFactory fileStorageFactory)
            : base(tokenHandler, configurationFactory, tenantTimeFactory)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            LoggerFactory = loggerFactory;
            CsvGenerator = csvGenerator;
            StatusManagementServiceFactory = statusManagementServiceFactory;
            ApplicationFilterClientFactory = applicationFilterClientFactory;
            ApplicationServiceClientFactory = applicationServiceClientFactory;
            EventHubFactory = eventHubFactory;

            MerchantCacheFactory = merchantCacheFactory;
            DocumentManagerServiceFactory = documentManagerServiceFactory;
            DataAttributesFactory = dataAttrbituesFactory;
            FileStorageFactory = fileStorageFactory;
        }

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ILoggerFactory LoggerFactory { get; }
        private ICsvGenerator CsvGenerator { get; }
        private IStatusManagementServiceFactory StatusManagementServiceFactory { get; }
        private IApplicationFilterClientFactory ApplicationFilterClientFactory { get; }
        private IApplicationServiceClientFactory ApplicationServiceClientFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private IMerchantCacheServiceFactory MerchantCacheFactory { get; }
        private IDocumentManagerServiceFactory DocumentManagerServiceFactory { get; }
        private IDataAttributesClientFactory DataAttributesFactory { get; }
        private IFileStorageFactory FileStorageFactory { get; }

        public override void OnSchedule()
        {
            var logger = LoggerFactory.CreateLogger();
            try
            {
                var token = TokenHandler.Issue(Tenant, Settings.ServiceName);
                var reader = new StaticTokenReader(token.Value);
                var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
                var statusManagementService = StatusManagementServiceFactory.Create(reader);
                var applicationFilterService = ApplicationFilterClientFactory.Create(reader);
                var applicationService = ApplicationServiceClientFactory.Create(reader);
                var eventHub = EventHubFactory.Create(reader);
                var merchantService = MerchantCacheFactory.Create(reader);
                var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
                var documentManagerService = DocumentManagerServiceFactory.Create(reader);
                var dataAttributes = DataAttributesFactory.Create(reader);
                var configuration = configurationService.Get();
                var fileStorageService = FileStorageFactory.Create(configuration.FileStorage, logger);

                new CreateOnBoardingFileGenerator(tenantTime, configuration, applicationService, applicationFilterService, statusManagementService, merchantService, logger, eventHub, CsvGenerator, documentManagerService, dataAttributes, fileStorageService)
                    .Init();
            }
            catch (Exception ex)
            {
                logger.Error("Error while executing the OnSchedule method from CreateOnBoardingFile task. Error: ", ex);
            }
        }
    }
}