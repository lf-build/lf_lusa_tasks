﻿namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public enum TransactionType
    {
        Undefined = 0,
        Checking = 1,
        Savings = 2,
        Others = 3
    }
}