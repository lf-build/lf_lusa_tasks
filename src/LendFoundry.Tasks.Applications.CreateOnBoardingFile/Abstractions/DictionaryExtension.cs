using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public static class DictionaryExtension
    {
        public static T TryGetValue<T>(this Dictionary<string, object> dictionary, string key)
        {
            try
            {
                object output = null;
                if (dictionary.TryGetValue(key, out output))
                {
                    return (T)Convert.ChangeType(output, typeof(T));
                }
                else if (dictionary.TryGetValue(key.ToLower(), out output))
                {
                    return (T)Convert.ChangeType(output, typeof(T));
                }
                return default(T);
            }
            catch (InvalidCastException)
            {
                return default(T);
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        public static TimeBucket TryGetTimeBucket<T>(this Dictionary<string, object> dictionary, string key)
        {
            try
            {
                var timeBucketResult = new TimeBucket();
                dynamic output = null;
                if (dictionary.TryGetValue(key, out output))
                {
                    timeBucketResult = new TimeBucket(new DateTimeOffset(Convert.ToDateTime(output.Time)));
                }
                else if (dictionary.TryGetValue(key.ToLower(), out output))
                {
                    timeBucketResult = new TimeBucket(new DateTimeOffset(Convert.ToDateTime(output.Time)));
                }
                return timeBucketResult;
            }
            catch (InvalidCastException)
            {
                return new TimeBucket();
            }
            catch (Exception)
            {
                return new TimeBucket();
            }
        }
    }
}