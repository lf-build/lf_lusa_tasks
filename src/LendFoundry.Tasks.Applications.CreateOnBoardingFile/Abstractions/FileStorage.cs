﻿namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public class FileStorage
    {       
        public string Host { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string PathOnboardFiles { get; set; }
        public string PathCreditPackageFiles { get; set; }
    }
}