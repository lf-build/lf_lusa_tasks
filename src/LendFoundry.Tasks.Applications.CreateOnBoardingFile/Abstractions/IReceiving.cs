﻿namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public interface IReceiving
    {
        string AccountNumber { get; set; }
        TransactionType AccountType { get; set; }
        string RoutingNumber { get; set; }
        string Name { get; set; }
    }
}