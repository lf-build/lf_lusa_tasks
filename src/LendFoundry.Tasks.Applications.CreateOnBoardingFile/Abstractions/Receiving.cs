﻿namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public class Receiving : IReceiving
    {
        public string AccountNumber { get; set; }
        public TransactionType AccountType { get; set; }
        public string RoutingNumber { get; set; }
        public string Name { get; set; }
    }
}