﻿using System;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public interface IRecoverabilityPolicy
    {
        bool CanBeRecoveredFrom(Exception exception);
    }
}