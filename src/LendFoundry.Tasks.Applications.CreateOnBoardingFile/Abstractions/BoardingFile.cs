﻿using LendFoundry.Merchant.Application;
using LendFoundry.Foundation.Services;
using LendFoundry.Merchant;
using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Merchant.Filters;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public class BoardingFile
    {
        public BoardingFile
        (
            IFilterView applicationFilter,
            IApplicationResponse application,
            IApplicantRequest applicant,
            Dictionary<string, object> dataAttributes,
            IMerchant merchant,
            IReceiving bankInfo,
            Amortization amortization
        )
        {
            if (applicant == null)
                throw new NotFoundException(nameof(applicant));

            if (dataAttributes == null)
                throw new NotFoundException(nameof(dataAttributes));

            MapFields(application, applicant, dataAttributes, merchant, bankInfo, amortization);
        }

        // LUSA Loan Purpose
        public string Fund { get; set; }
        // LUSA Loan Purpose
        public string AssetType { get; set; }
        // Name of the merchant
        public string Merchant { get; set; }
        // Borrower ID
        public string InternalAccountNumber { get; set; }
        // Borrower Social Security Number
        public string BorrowerSsn { get; set; }
        // Borrower Last Name
        public string BorrowerLastName { get; set; }
        // Borrower First Name
        public string BorrowerFirstName { get; set; }
        // Borrower Date of Birth
        public string Dob { get; set; }
        // Borrower Street Address (Must be physical address; not a PO Box)
        public string BorrowerAddr { get; set; }
        // Borrower  City 
        public string BorrowerAddrCity { get; set; }
        // Borrower State 
        public string BorrowerAddrState { get; set; }
        // Borrower Zip code (5 digits)
        public string BorrowerAddrZipCode { get; set; }
        // Borrower phone number 1
        public string BorrowerPhone1 { get; set; }
        // Borrower phone number 2
        public string BorrowerPhone2 { get; set; }
        // Borrower phone number 3
        public string BorrowerPhone3 { get; set; }
        // Borrower Email Address
        public string BorrowerEmailAddress { get; set; }
        // Keep Blank
        public string CoBorrowerLastName { get; set; }
        // Keep Blank
        public string CoBorrowerFirstName { get; set; }
        // Keep Blank
        public string CoBorrowerSsn { get; set; }
        // Keep Blank 
        public string CoBorrowerDob { get; set; }
        // Keep Blank 
        public string CoBorrowerAddr { get; set; }
        // Keep Blank 
        public string CoBorrowerAddrCity { get; set; }
        // Keep Blank 
        public string CoBorrowerAddrState { get; set; }
        // Keep Blank 
        public string CoBorrowerAddrZipCode { get; set; }
        // Keep Blank 
        public string CoBorrowerPhone1 { get; set; }
        // Keep Blank 
        public string CoBorrowerPhone2 { get; set; }
        // Keep Blank 
        public string CoBorrowerPhone3 { get; set; }
        // Keep Blank 
        public string CoBorrowerEmailAddress { get; set; }
        // LUSA Loan Purpose
        public string ItemFinanced { get; set; }
        // Amount financed + origination fee
        public double LoanAmount { get; set; }
        // Date of origination of the loan
        public string OriginationDate { get; set; }
        // Date of first repayment
        public string FirstPaymentDate { get; set; }
        // Number of Months at origination to pay off the loan 
        public double ContractTermInYears { get; set; }
        // Amount to be paid monthly
        public double MonthlyPaymentAmount { get; set; }
        // Interest rate
        public string InterestRate { get; set; }
        // Default FP , Changeable
        public string InterestFreeMethod { get; set; }
        // Default Monthly
        public string InterestFreePeriod { get; set; }
        // 6 for now, may be 12 in future
        public int InterestFreeNoOfPeriods { get; set; }
        // Default 16 , Changeable
        public double InterestFreeMaxDpdInGracePeriod { get; set; }
        // Default 0 , Changeable
        public double InterestFreePayoffGrace { get; set; }
        // Frequency of repayments, default Monthly
        public string PaymentFrequency { get; set; }
        // Default 16 , Changeable
        public double GracePeriod { get; set; }
        // Default $5 , Changeable
        public double LateFee { get; set; }
        // Default 10 , Changeable
        public double NsfFee { get; set; }
        // Bank Routing Number
        public string RoutingNumber { get; set; }
        // Bank Account Number
        public string AccountNumber { get; set; }
        // Account type, If routing# and account# is available then account type is required.
        public string AccountType { get; set; }
        // Indicate if loan is being made to a 'covered borrower'
        public string MlaEligible { get; set; }
        // MAPR for loan to covered borrower, as applicable, using defintion from regulation 
        public string Mapr { get; set; }
        // Will be the credit grade from the funding tape.
        public string RiskRating { get; set; }
        // Default date time format to creating CSV file
        private const string DateFormatIn = "MM/dd/yyyy";

        private void MapFields(IApplicationResponse application, IApplicantRequest applicant, Dictionary<string, object> dataAttributes, IMerchant merchant, IReceiving bankInfo, Amortization amortization)
        {
            Fund = application.Purpose;
            AssetType = application.Purpose;
            Merchant = merchant?.BusinessName;
            InternalAccountNumber = application.ApplicationNumber;
            BorrowerSsn = applicant.Ssn;
            BorrowerLastName = applicant.LastName;
            BorrowerFirstName = applicant.FirstName;
            Dob = applicant.DateOfBirth.ToString(DateFormatIn);
            SetBorrowerData(applicant);
            CoBorrowerLastName = "";
            CoBorrowerFirstName = "";
            CoBorrowerSsn = "";
            CoBorrowerDob = "";
            CoBorrowerAddr = "";
            CoBorrowerAddrCity = "";
            CoBorrowerAddrState = "";
            CoBorrowerAddrZipCode = "";
            CoBorrowerPhone1 = "";
            CoBorrowerPhone2 = "";
            CoBorrowerPhone3 = "";
            CoBorrowerEmailAddress = "";
            ItemFinanced = application.Purpose;
            LoanAmount = amortization.AmountFinanced;

            // CRB Funding Date
            OriginationDate = dataAttributes.TryGetValue<DateTime>("fundingOriginationDate").ToString(DateFormatIn);

            // 30 Calendar days from the CRB Funding Date
            FirstPaymentDate = amortization.FirstPaymentDate.ToString(DateFormatIn);

            // Term of the Loan / 12 (e.g. 36 month loan / 12 months = 3)"
            ContractTermInYears = amortization.PaymentCount / 12;

            // Monthly payment according to majority of payment. Same as the Monthly Payment Amount from the funding tape
            MonthlyPaymentAmount = amortization.FirstPaymentAmount;

            InterestRate = dataAttributes.TryGetValue<double>("interestRate").ToString("#.000");
            InterestFreeMethod = "FP";
            InterestFreePeriod = "MONTHLY";
            InterestFreeNoOfPeriods = 6;
            InterestFreeMaxDpdInGracePeriod = 16;
            InterestFreePayoffGrace = 0;
            PaymentFrequency = "MONTHLY";
            GracePeriod = 16;
            LateFee = 5;
            NsfFee = 10;

            // Only fill bank if it`s auto pay.
            if (dataAttributes.TryGetValue<bool>("isAutoPay"))
            {
                RoutingNumber = dataAttributes.TryGetValue<string>("autoPayBankRoutingNumber");
                AccountNumber = dataAttributes.TryGetValue<string>("autoPayBankAccountNumber");
                AccountType = dataAttributes.TryGetValue<string>("autoPayBankAccountType");
            }

            // MAPR for loan to covered borrower, as applicable, using defintion from regulation 
            // if Not military, value = NULL, if military then MAPR = APR
            MlaEligible = dataAttributes.TryGetValue<bool>("mlaflag") ? "Y" : "N";
            Mapr = MlaEligible != "Y" ? "NULL" : dataAttributes.TryGetValue<double>("apr").ToString();
            RiskRating = dataAttributes.TryGetValue<string>("creditGrade");
        }

        private void SetBorrowerData(IApplicantRequest applicant)
        {
            var address = applicant.Addresses?.FirstOrDefault(p => p.IsPrimary);
            if (address != null)
            {
                BorrowerAddr = ($"{ address.Line1}{ address.Line2}{ address.Line3}").ToUpper();
                BorrowerAddrCity = address.City.ToUpper();
                BorrowerAddrState = address.State.ToUpper();
                BorrowerAddrZipCode = address.ZipCode;
            }

            applicant.PhoneNumbers?
                     .ToList()
                     .Select((p, idx) =>
                     {
                         if (idx == 0) { BorrowerPhone1 = p.Number; }

                         // ATTENTION: 
                         // For now only phone number 1 will be provided
                         // as discussed with customer.
                         //if (idx == 1) { BorrowerPhone2 = p.Number; }
                         //if (idx == 2) { BorrowerPhone3 = p.Number; }
                         return p;
                     }).ToList();

            // Borrower Email Address
            BorrowerEmailAddress = applicant.Email;
        }
    }
}