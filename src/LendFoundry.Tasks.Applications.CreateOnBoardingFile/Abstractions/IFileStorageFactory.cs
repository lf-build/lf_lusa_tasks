﻿using LendFoundry.Foundation.Logging;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public interface IFileStorageFactory
    {
        IFileStorageService Create(FileStorage fileStorage, ILogger logger);
    }
}