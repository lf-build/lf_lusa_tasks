﻿using LendFoundry.Merchant;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public interface IMerchantCacheService
    {
        IMerchant Get(string merchantId);

        void Clear();
    }
}