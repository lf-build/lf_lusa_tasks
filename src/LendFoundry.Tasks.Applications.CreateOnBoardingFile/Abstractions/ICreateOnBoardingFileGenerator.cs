﻿namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public interface ICreateOnBoardingFileGenerator
    {
        void Init();
    }
}