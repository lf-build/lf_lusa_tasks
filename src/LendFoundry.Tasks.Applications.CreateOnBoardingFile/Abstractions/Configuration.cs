﻿using System.Collections.Generic;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public class Configuration
    {
        public FileStorage FileStorage { get; set; }
        public string CreditPackageFileName { get; set; }
        public string[] FundedStatusCode { get; set; }
        public InServiceStatus InServiceStatus { get; set; }
        public int NumberOfDays { get; set; }                     
        public Dictionary<string, string> CsvProjection { get; set; }        
    }
}