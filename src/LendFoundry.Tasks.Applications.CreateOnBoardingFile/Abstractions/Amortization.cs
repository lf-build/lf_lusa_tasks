﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public class Amortization
    {
        public double InterestRate { get; set; }
        public double Apr { get; set; }
        public double OfferAmount { get; set; }
        public double OriginationAmount { get; set; }
        public double NoteAmount { get; set; }
        public double FinanceCharges { get; set; }
        public double AmountFinanced { get; set; }
        public DateTime FirstPaymentDate { get; set; }
        public double FirstPaymentAmount { get; set; }
        public DateTime LastPaymentDate { get; set; }
        public double LastPaymentAmount { get; set; }
        public int PaymentCount { get; set; }
        public int PaymentCountMinusOne { get; set; }
        public double PaymentAmount { get; set; }
    }
}
