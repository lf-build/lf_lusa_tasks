﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public interface IMerchantCacheServiceFactory
    {
        IMerchantCacheService Create(ITokenReader reader);
    }
}