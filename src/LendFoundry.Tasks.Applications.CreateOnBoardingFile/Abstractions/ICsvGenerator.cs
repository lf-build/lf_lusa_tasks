﻿using System.Collections.Generic;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public interface ICsvGenerator
    {
        byte[] Write<T>(IEnumerable<T> items, Dictionary<string, string> csvProjection);
    }
}