﻿namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public class InServiceStatus
    {
        public string Code { get; set; }
        public string Reason { get; set; }
    }
}