﻿using LendFoundry.Merchant.Application.Client;
using LendFoundry.Merchant.Applications.Filters.Client;
using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.DocumentManager.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Merchant.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement.Client;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public class Program : DependencyInjection
    {
        public void Main(string[] args)
        {
            Provider.GetService<IAgent>().Execute();
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddCalendarService(Settings.Calendar.Host, Settings.Calendar.Port);
            services.AddConfigurationService<Configuration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddStatusManagementService(Settings.StatusManagement.Host, Settings.StatusManagement.Port);
            services.AddApplicationFilters(Settings.ApplicationFilter.Host, Settings.ApplicationFilter.Port);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddApplicationService(Settings.Application.Host, Settings.Application.Port);
            services.AddDocumentManager(Settings.DocumentManager.Host, Settings.DocumentManager.Port);
            services.AddMerchant(Settings.Merchant.Host, Settings.Merchant.Port);
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddTransient<IAgent, Agent>();
            services.AddTransient<ICsvGenerator, CsvGenerator>();
            services.AddTransient<IMerchantCacheServiceFactory, MerchantCacheServiceFactory>();
            services.AddDataAttributes(Settings.DataAttributes.Host, Settings.DataAttributes.Port);
            services.AddTransient<IFileStorageFactory, FileStorageFactory>();
            return services;
        }
    }
}