﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public class CsvGenerator : ICsvGenerator
    {
        private Dictionary<string, string> CsvProjection { get; set; }

        public byte[] Write<T>(IEnumerable<T> items, Dictionary<string, string> csvProjection)
        {
            try
            {
                CsvProjection = csvProjection;
                using (var memoryStream = new MemoryStream())
                {
                    using (var streamWriter = new StreamWriter(memoryStream))
                    using (var csvWriter = new CsvWriter(streamWriter, SetCsvProjectionValues()))
                    {
                        csvWriter.WriteRecords(items);
                    } // StreamWriter gets flushed here.

                    return memoryStream.ToArray();
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private CsvConfiguration SetCsvProjectionValues()
        {
            var csvConfiguration = new CsvConfiguration()
            {
                Encoding = Encoding.UTF8,
            };

            if (CsvProjection != null && CsvProjection.Any())
                csvConfiguration.RegisterClassMap(SetCsvClassMap());
            return csvConfiguration;
        }

        private CsvClassMap SetCsvClassMap()
        {
            var boardingFileMap = new DefaultCsvClassMap<BoardingFile>();
            CsvClassMap BoardingFileMap;
            Dictionary<string, string> csvProjection = CsvProjection;
            var columnIndex = 0;
            foreach (var csvKeyColumn in csvProjection)
            {
                var schemaPropertyName = csvKeyColumn.Key;
                var csvColumnName = csvKeyColumn.Value;
                try
                {  
                    if (!String.IsNullOrEmpty(csvColumnName))
                    {
                        var propertyInfo = typeof(BoardingFile).GetProperty(schemaPropertyName);
                        var newMap = new CsvPropertyMap(propertyInfo);
                        newMap.Name(csvColumnName);
                        newMap.Index(columnIndex);
                        boardingFileMap.PropertyMaps.Add(newMap);
                        columnIndex += 1;
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            BoardingFileMap = boardingFileMap;
            return BoardingFileMap;
        }
    }
}