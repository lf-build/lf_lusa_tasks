﻿using System;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public class AlwaysRetryPolicy : IRecoverabilityPolicy
    {
        public bool CanBeRecoveredFrom(Exception ex)
        {
            return true;
        }
    }
}