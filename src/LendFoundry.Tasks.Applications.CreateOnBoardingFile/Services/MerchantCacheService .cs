﻿using LendFoundry.Foundation.Services;
using LendFoundry.Merchant;
using System.Collections.Concurrent;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public class MerchantCacheService : IMerchantCacheService
    {
        private IMerchantService MerchantService { get; }
        private ConcurrentDictionary<string, IMerchant> Merchants { get; }

        public MerchantCacheService(IMerchantService merchantService)
        {
            MerchantService = merchantService;
            Merchants = new ConcurrentDictionary<string, IMerchant>();
        }

        public IMerchant Get(string merchantId)
        {
            IMerchant merchant;
            if (Merchants.TryGetValue(merchantId, out merchant))
                return merchant;

            merchant = MerchantService.Get(merchantId);
            if (merchant == null)
                throw new NotFoundException($"Merchant {merchantId} not found");
            Merchants.TryAdd(merchantId, merchant);

            return merchant;
        }

        public void Clear()
        {
            Merchants.Clear();
        }
    }
}