﻿using LendFoundry.Merchant.Application;
//using LendFoundry.Merchant.Applications.Filters;
using LendFoundry.DataAttributes;
using LendFoundry.DocumentManager;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Merchant;
using LendFoundry.StatusManagement;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.EventHub;
using LendFoundry.Merchant.Filters;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public class CreateOnBoardingFileGenerator : ICreateOnBoardingFileGenerator
    {
        public CreateOnBoardingFileGenerator
        (
            ITenantTime tenantTime,
            Configuration configuration,
            IApplicationService applicationService,
            IApplicationFilterService applicationFilterService,
            IEntityStatusService statusManagementService,
            IMerchantCacheService merchantCacheService,
            ILogger logger,
            IEventHubClient eventHub,
            ICsvGenerator csvGenerator,
            IDocumentManagerService documentManagerService,
            IDataAttributesEngine dataAttributes,
            IFileStorageService fileStorageService
        )
        {
            if (tenantTime == null)
                throw new ArgumentException($"{nameof(tenantTime)} is mandatory");

            if (configuration == null)
                throw new ArgumentException($"{nameof(configuration)} is mandatory");

            if (logger == null)
                throw new ArgumentException($"{nameof(logger)} is mandatory");

            if (csvGenerator == null)
                throw new ArgumentException($"{nameof(csvGenerator)} is mandatory");

            if (applicationService == null)
                throw new ArgumentException($"{nameof(applicationService)} is mandatory");

            if (applicationFilterService == null)
                throw new ArgumentException($"{nameof(applicationFilterService)} is mandatory");

            if (statusManagementService == null)
                throw new ArgumentException($"{nameof(statusManagementService)} is mandatory");

            if (merchantCacheService == null)
                throw new ArgumentException($"{nameof(merchantCacheService)} is mandatory");

            if (documentManagerService == null)
                throw new ArgumentException($"{nameof(documentManagerService)} is mandatory");

            if (dataAttributes == null)
                throw new ArgumentException($"{nameof(dataAttributes)} is mandatory");

            if (fileStorageService == null)
                throw new ArgumentException($"{nameof(fileStorageService)} is mandatory");

            TenantTime = tenantTime;
            Logger = logger;
            CsvGenerator = csvGenerator;
            Configuration = configuration;
            ApplicationService = applicationService;
            ApplicationFilterService = applicationFilterService;
            StatusManagementService = statusManagementService;
            MerchantCacheService = merchantCacheService;
            EventHub = eventHub;
            DocumentManagerService = documentManagerService;
            DataAttributes = dataAttributes;
            FileStorageService = fileStorageService;
        }

        private Configuration Configuration { get; }
        private ITenantTime TenantTime { get; }
        private ILogger Logger { get; }
        private ICsvGenerator CsvGenerator { get; }
        private IApplicationService ApplicationService { get; }
        private IApplicationFilterService ApplicationFilterService { get; }
        private IEntityStatusService StatusManagementService { get; }
        private IMerchantCacheService MerchantCacheService { get; }
        private IEventHubClient EventHub { get; }
        private IDocumentManagerService DocumentManagerService { get; }
        private IDataAttributesEngine DataAttributes { get; }
        private IFileStorageService FileStorageService { get; }

        public async void Init()
        {
            if (!EnsureConfigurationIsValid(Configuration, Logger)) return;

            Logger.Info("Starting task FA Onboarding file process");
            Logger.Info($"Fetching all Funded applications {Configuration.NumberOfDays} day(s) ago");

            var applications = await ApplicationFilterService.GetByStatus(Configuration.FundedStatusCode);

            //TODO: IN ORDER TO TEST AND SHOW THE FILE CREATION IN REAL TIME TO THE CUSTOMER
            //      WE ARE NOT VALIDATING FUNDED DAYS TO GENERATE ONBOARD FILE
            //      PLEASE, REMOVE THE COMMENT FOR DATA FILTER TO MAKE REQUIREMENT WORKS AGAIN.

            // Filter only application funded X days ago (based on configuration)
            //applications = applications?.Where(p => p.ActiveOn != null && p.ActiveOn.Time <= TenantTime.Today.AddDays(-Configuration.NumberOfDays)).ToList();

            // In case of no application task should stop
            if (applications == null || !applications.Any())
            {
                Logger.Info("No applications found");
                FinishTaskLog();
                return;
            }

            #region  -- Generate Onboard files --
            Logger.Info($"Total of #{applications.Count()} application(s) found");
            Logger.Info("Generating Boarding files...");
            var boardingFiles = new List<BoardingFile>();
            applications.ToList().ForEach(application =>
            {
                boardingFiles.Add(BuildBoardingFileFor(application));
                Logger.Info($"application #{application.ApplicationId} data generated successfully");
            });

            if (boardingFiles.Any())
            {
                try
                {
                    using (var csvMemoryStream = new MemoryStream(CsvGenerator.Write(boardingFiles, Configuration.CsvProjection)))
                    {
                        var fileName = $"First_Associate_Loan_Boarding_File_{TenantTime.Now:yyyy-MM-dd}.csv";
                        FileStorageService.Upload(csvMemoryStream, fileName, Configuration.FileStorage.PathOnboardFiles);
                        Logger.Info("Boarding files uploaded successfully");
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error($"Error while uploading boarding file to sftp: ", ex);
                    EventHub.Publish(new SftpFileUploadFailed()).Wait();
                    FinishTaskLog();
                    return;
                }
            }
            #endregion

            #region -- Generate Credit package files --
            Logger.Info("Generating Loan Packages...");
            applications.ToList().ForEach(application =>
            {
                try
                {
                    if (!string.IsNullOrWhiteSpace(application.ApplicationId))
                    {
                        var filesToUpload = GetApplicationCreditPackage(application.ApplicationId).Result;
                        filesToUpload?.ToList().ForEach(file =>
                        {
                            using (var fileMemoryStream = new MemoryStream())
                            {
                                byte[] buffer = new byte[16 * 1024];
                                int read;
                                while ((read = file.Value.Read(buffer, 0, buffer.Length)) > 0)
                                {
                                    fileMemoryStream.Write(buffer, 0, read);
                                }

                                var pathToUpload = $"{Configuration.FileStorage.PathCreditPackageFiles}/{TenantTime.Now:yyyy-MM-dd}";
                                if (!FileStorageService.Exist(pathToUpload))
                                    FileStorageService.CreateDirectory(pathToUpload);

                                FileStorageService.Upload(fileMemoryStream, file.Key, pathToUpload);
                                Logger.Info($"Package generated for application #{application.ApplicationId}");
                            }
                        });

                        EventHub.Publish(new ApplicationOnBoardingFileCreated { Application = application }).Wait();
                        ChangeApplicationStatus(application);
                        Logger.Info("Loan Package files uploaded successfully");
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error($"Error while upload to sftp client for {application.Id}", ex);
                    EventHub.Publish(new ApplicationOnBoardingFileFailed { Application = application }).Wait();
                }
            });
            #endregion

            FinishTaskLog();
        }

        private void ChangeApplicationStatus(IFilterView application)
        {
            try
            {
                var inServiceStatus = Configuration.InServiceStatus;
                StatusManagementService.ChangeStatus("application", application.ApplicationId, inServiceStatus.Code, inServiceStatus.Reason);
            }
            catch (Exception)
            {
                Logger.Error($"Error while changing status for application {application.ApplicationId}");
            }
        }

        private bool EnsureConfigurationIsValid(Configuration configuration, ILogger logger)
        {
            if (configuration == null)
            {
                logger.Error("Did not find the configuration related to applications - create onboarding file task");
                return false;
            }
            if (configuration.FundedStatusCode == null || !configuration.FundedStatusCode.Any())
            {
                logger.Error("Did not find any funded status code.");
                return false;
            }
            if (configuration.NumberOfDays <= 0)
            {
                logger.Error("NumberOfDays should have the value greater than the zero");
                return false;
            }
            if (string.IsNullOrWhiteSpace(configuration.InServiceStatus?.Code))
            {
                logger.Error("Did not find any in service status code.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(configuration.CreditPackageFileName))
            {
                logger.Error("Did not find credit package file name.");
                return false;
            }
            if (configuration.CsvProjection == null || !configuration.CsvProjection.Any())
            {
                logger.Error("Did not find csv projection header values.");
                return false;
            }
            if (configuration.FileStorage == null)
            {
                logger.Error("Did not find file storage values.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(configuration.FileStorage.Host))
            {
                logger.Error("Did not find file storage host value.");
                return false;
            }
            if (configuration.FileStorage.Port <= 0)
            {
                logger.Error("File storage port value must be greater than zero.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(configuration.FileStorage.Username))
            {
                logger.Error("Did not find file storage username value.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(configuration.FileStorage.Password))
            {
                logger.Error("Did not find file storage password value.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(configuration.FileStorage.PathOnboardFiles))
            {
                logger.Error("Did not find file storage path value.");
                return false;
            }
            return true;
        }

        private BoardingFile BuildBoardingFileFor(IFilterView applicationFilter)
        {
            try
            {
                var merchant = default(IMerchant);
                var application = default(IApplicationResponse);
                var applicant = default(IApplicantRequest);
                var dataAttributes = default(Dictionary<string, object>);
                var bankInfo = default(IReceiving);

                merchant = MerchantCacheService.Get(applicationFilter.SourceId);
                application = ApplicationService.GetByApplicationNumber(applicationFilter.ApplicationId);
                applicant = application.Applicants?.FirstOrDefault(x => x.IsPrimary);
                dataAttributes = DataAttributes.GetAllAttributes("application", applicationFilter.ApplicationId).Result?.ToDictionary(k => k.Key, v => v.Value);
                bankInfo = GetBankAccountDetails(applicationFilter, application, merchant);
                Amortization amortization = null;
                if (dataAttributes.Count() > 0 && dataAttributes.Keys.Contains("amortization") && dataAttributes["amortization"] != null)
                    amortization = JObject.FromObject(dataAttributes["amortization"]).ToObject<Amortization>();

                return new BoardingFile(applicationFilter, application, applicant, dataAttributes, merchant, bankInfo, amortization);
            }
            catch (Exception ex)
            {
                Logger.Error($"Error while parsing values to application #{applicationFilter.ApplicationId}", ex);
                EventHub.Publish(new ApplicationOnBoardingFileFailed { Application = applicationFilter }).Wait();
            }
            return null;
        }

        private IReceiving GetBankAccountDetails(IFilterView applicationFilterView, IApplicationResponse application, IMerchant merchant)
        {
            var bankInfo = default(IReceiving);

            if (applicationFilterView.SourceType.Equals("merchant", StringComparison.InvariantCultureIgnoreCase))
            {
                if (merchant.BankAccounts != null && merchant.BankAccounts.Any())
                {
                    var bankAccountDetails = merchant.BankAccounts.FirstOrDefault(p => p.IsPrimary);
                    var instructionType = (TransactionType)bankAccountDetails.AccountType;

                    bankInfo = new Receiving
                    {
                        AccountNumber = bankAccountDetails.AccountNumber,
                        AccountType = instructionType,
                        Name = bankAccountDetails.AccountHolderName,
                        RoutingNumber = bankAccountDetails.RoutingNumber
                    };
                }
            }

            if (applicationFilterView.SourceType.Equals("application", StringComparison.InvariantCultureIgnoreCase))
            {
                if (application == null)
                    throw new NotFoundException($"Application {applicationFilterView.Id} not found");

                if (application.BankAccounts == null)
                    throw new NotFoundException(
                        $"Application {applicationFilterView.SourceId} with bank account was not found");
                var bankAccountDetails = application.BankAccounts.FirstOrDefault(p => p.IsPrimary);
                if (bankAccountDetails == null)
                    throw new NotFoundException("Bank account not found for the merchant");

                var instructionType = (TransactionType)bankAccountDetails.AccountType;

                var primaryApplicant = application.Applicants.FirstOrDefault(applicant => applicant.IsPrimary);
                if (primaryApplicant == null)
                    throw new NotFoundException("Primary Applicant not found");

                bankInfo = new Receiving
                {
                    AccountNumber = bankAccountDetails.AccountNumber,
                    AccountType = instructionType,
                    Name = primaryApplicant.FirstName + primaryApplicant.LastName,
                    RoutingNumber = bankAccountDetails.RoutingNumber
                };
            }
            return bankInfo;
        }

        private async Task<Dictionary<string, Stream>> GetApplicationCreditPackage(string applicationId)
        {
            const string entityType = "application";
            Dictionary<string, Stream> files = new Dictionary<string, Stream>();
            var documentList = (await DocumentManagerService.GetAllLatest(entityType, applicationId)).ToList();

            if (!documentList.Any())
                return null;

            return await Task.Run(async () =>
            {
                // Check if all mandatory fields exists and upload it.
                var creditPackageFileName = $"LCUSA-{applicationId}-LD.pdf";
                var creditPackageFileData = documentList.FirstOrDefault(d => d.FileName.StartsWith(creditPackageFileName, StringComparison.Ordinal));
                if (creditPackageFileData != null)
                {
                    var fdata = await DocumentManagerService.Download(entityType, applicationId, creditPackageFileData.Id);
                    files.Add(creditPackageFileName, fdata);
                }

                var lexisNexisInstandIdFileName = $"LCUSA-{applicationId}-LNI.pdf";
                var lexisNexisInstandIdFileData = documentList.FirstOrDefault(d => d.FileName.StartsWith(lexisNexisInstandIdFileName, StringComparison.Ordinal));
                if (lexisNexisInstandIdFileData != null)
                {
                    var fdata = await DocumentManagerService.Download(entityType, applicationId, lexisNexisInstandIdFileData.Id);
                    files.Add(lexisNexisInstandIdFileName, fdata);
                }

                var creditReportFileName = $"LCUSA-{applicationId}-CR.pdf";
                var creditReportFileData = documentList.FirstOrDefault(d => d.FileName.StartsWith(creditReportFileName, StringComparison.Ordinal));
                if (creditReportFileData != null)
                {
                    var fdata = await DocumentManagerService.Download(entityType, applicationId, creditReportFileData.Id);
                    files.Add(creditReportFileName, fdata);
                }
                return files;
            });
        }

        private void FinishTaskLog()
        {
            Logger.Info("Task finished");
            Logger.Info("--------------------------------------------");
        }
    }
}