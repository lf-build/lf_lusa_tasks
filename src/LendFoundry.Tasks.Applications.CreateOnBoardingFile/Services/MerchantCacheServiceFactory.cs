﻿using LendFoundry.Merchant.Client;
using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public class MerchantCacheServiceFactory : IMerchantCacheServiceFactory
    {
        public MerchantCacheServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IMerchantCacheService Create(ITokenReader reader)
        {
            var merchantServiceFactory = Provider.GetService<IMerchantServiceFactory>();
            var merchantService = merchantServiceFactory.Create(reader);
            return new MerchantCacheService(merchantService);
        }
    }
}