﻿using LendFoundry.Foundation.Logging;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public class FileStorageFactory : IFileStorageFactory
    {
        public IFileStorageService Create(FileStorage fileStorage, ILogger logger)
        {
            return new FileStorageService(fileStorage, logger);
        }
    }
}