﻿using LendFoundry.Configuration.Client;
using LendFoundry.Email.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Merchant.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement;
using LendFoundry.Tasks.Agent;
using System;
using System.Linq;

namespace LendFoundry.Tasks.Merchants.SendEmailsOnEnrolledActive
{
    public class Agent : EventBasedAgent
    {
        public Agent
        (
            IEventHubClientFactory eventHubClientFactory,
            IMerchantServiceFactory merchantServiceFactory,
            ITokenReaderFactory tokenHandlerFactory,
            IEmailServiceFactory emailServiceFactory,
            ILoggerFactory loggerFactory,
            ITokenHandler tokenHandler,
            IConfigurationServiceFactory configurationFactory = null
        )
            : base(eventHubClientFactory, Settings.ServiceName, Settings.EventName, tokenHandler, loggerFactory)
        {
            MerchantServiceFactory = merchantServiceFactory;
            TokenHandlerFactory = tokenHandlerFactory;
            EmailServiceFactory = emailServiceFactory;
            ConfigurationFactory = configurationFactory;
        }

        private IMerchantServiceFactory MerchantServiceFactory { get; }
        private ITokenReaderFactory TokenHandlerFactory { get; }
        private IEmailServiceFactory EmailServiceFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }

        public override async void Execute(EventInfo @event)
        {            
            var reader = TokenHandlerFactory.Create(@event.TenantId, string.Empty);
            var merchantService = MerchantServiceFactory.Create(reader);
            var emailService = EmailServiceFactory.Create(reader);
            var logger = LoggerFactory.CreateLogger();
            var configuration = ConfigurationFactory.Create<MerchantSendEmailsOnEnrolledActiveConfiguration>(Settings.ServiceName, reader).Get();

            logger.Info("Email info data collection started");
            try
            {
                var statusChangedEvent = EnsureValidEvent(@event, logger);
                if (statusChangedEvent == null)
                {
                    logger.Error($"Received event is invalid");
                    return;
                }

                if (configuration == null)
                {
                    logger.Error($"The configuration related to {Settings.ServiceName} was not found");
                    return;
                }

                if (statusChangedEvent.OldStatus == configuration.ConditionalEnrolmentStatusCode && statusChangedEvent.NewStatus == configuration.EnrolledActiveStatusCode)
                {
                    logger.Info($"Status code has been changed from {configuration.ConditionalEnrolmentStatusCode} to {configuration.EnrolledActiveStatusCode}");

                    // status is EnrolledActive, send the emails to the merchant
                    var entityId = statusChangedEvent.EntityId;
                    var entityType = statusChangedEvent.EntityType;

                    logger.Info($"Received request to EntityType: {entityType}, and EntityId: {entityId}");

                    //get merchant details
                    var merchant = merchantService.Get(entityId);

                    //welcome kit for shipment
                    var link = string.Format(Settings.Link ?? "link not available for: {0}", merchant.MerchantId);
                    var welcomeRequest = new
                    {
                        TemplateName = configuration.WelcomeTemplateName,
                        TemplateVersion = configuration.WelcomeTemplateVersion,
                        Email = merchant.Email,
                        MerchantContact = merchant.Contacts?.FirstOrDefault().FirstName,
                        CustomerEmail = merchant.Email,
                        ActionLink = link
                    };
                    await emailService.Send(configuration.WelcomeTemplateName, configuration.WelcomeTemplateVersion, welcomeRequest);
                    logger.Info($"Welcome Notification sent to {merchant.Email} - {entityType}: {entityId}");

                    //welcome mail from CEO
                    var birdeyeLink = string.Format(Settings.BirdEyeSurveyLink ?? "link not available for: {0}", merchant.MerchantId);
                    var ceoRequest = new
                    {
                        TemplateName = configuration.CeoTemplateName,
                        TemplateVersion = configuration.CeoTemplateVersion,
                        Email = merchant.Email,
                        MerchantBusinessName = merchant.BusinessName,
                        MerchantPortalAddress = configuration.MerchantPortalAddress,
                        CustomerEmail = merchant.Email,
                        ActionLink = birdeyeLink
                    };
                    await emailService.Send(configuration.CeoTemplateName, configuration.CeoTemplateVersion, ceoRequest);
                    logger.Info($"Ceo Notification sent to {merchant.Email} - {entityType}: {entityId}");
                }
            }
            catch (Exception e)
            {
                logger.Error($"Exception on event {@event.Id} - {@event.Name}", e);
            }
        }

        private StatusChanged EnsureValidEvent(EventInfo @event, ILogger logger)
        {
            if (@event.Data == null)
            {
                logger.Error($"Cast to {nameof(StatusChanged)} cannot is possible");
                return null;
            }
            var statusChangedEvent = @event.Cast<StatusChanged>();

            if (!"merchant".Equals(statusChangedEvent.EntityType, StringComparison.InvariantCultureIgnoreCase))
            {
                logger.Error($"Skipping for Entity Type {statusChangedEvent.EntityType}. Only merchant entityType is allowed.");
                return null;
            }

            if (string.IsNullOrWhiteSpace(statusChangedEvent.EntityId))
            {
                logger.Error("EntityId is null or empty");
                return null;
            }

            return statusChangedEvent;
        }
    }
}
