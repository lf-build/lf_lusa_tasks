﻿namespace LendFoundry.Tasks.Merchants.SendEmailsOnEnrolledActive
{
    public class MerchantSendEmailsOnEnrolledActiveConfiguration
    {
        public string EnrolledActiveStatusCode { get; set; }

        public string ConditionalEnrolmentStatusCode { get; set; }

        public string EntityId { get; set; }

        public string EntityType { get; set; }

        public string EventName { get; set; }

        public string WelcomeTemplateName { get; set; }

        public string WelcomeTemplateVersion { get; set; }

        public string CeoTemplateName { get; set; }

        public string CeoTemplateVersion { get; set; }

        public string MerchantPortalAddress { get; set; }
    }
}
