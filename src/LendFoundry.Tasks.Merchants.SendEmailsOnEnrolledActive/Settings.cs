﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Tasks.Merchants.SendEmailsOnEnrolledActive
{
    public class Settings
    {
        public const string Prefix = "TASK_MERCHANT_SENDEMAILSONENROLLEDACTIVE";

        public static string ServiceName { get; } = Prefix.ToLower();

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");

        public static ServiceSettings Merchant { get; } = new ServiceSettings($"{Prefix}_MERCHANT_HOST", "merchant", $"{Prefix}_MERCHANT_PORT");

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB_HOST", "eventhub", $"{Prefix}_EVENTHUB_PORT");

        public static ServiceSettings Email { get; } = new ServiceSettings($"{Prefix}_EMAIL_HOST", "email", $"{Prefix}_EMAIL_PORT");

        public static string Link = Environment.GetEnvironmentVariable($"{Prefix}_LINK");

        public static string BirdEyeSurveyLink = Environment.GetEnvironmentVariable($"{Prefix}_BIRDEYELINK");

        public static string EventName = Environment.GetEnvironmentVariable($"{Prefix}_EVENT_NAME");

        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats://nats:4222";
    }
}
