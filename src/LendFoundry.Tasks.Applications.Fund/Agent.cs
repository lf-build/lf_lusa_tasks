using System;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Merchant.Application;
using LendFoundry.Merchant.Application.Client;
using LendFoundry.Merchant.Applications.Filters;
using LendFoundry.Merchant.Applications.Filters.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.MoneyMovement.Ach;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement.Client;
using LendFoundry.Tasks.Agent;
using AccountType = LendFoundry.MoneyMovement.Ach.AccountType;
using LendFoundry.DataAttributes.Client;

namespace LendFoundry.Tasks.Applications.Fund
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory, 
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory, 
            IAchServiceFactory achServiceFactory, 
            ILoggerFactory loggerFactory,
            IStatusManagementServiceFactory statusManagementServiceFactory,
            IApplicationFilterClientFactory applicationFilterClientFactory,
            IApplicationServiceClientFactory applicationServiceClientFactory, 
            IMerchantCacheServiceFactory merchantCacheFactory,
            IEventHubClientFactory eventHubClientFactory,
              IDataAttributesClientFactory dataAttrbituesFactory)
            : base(tokenHandler, configurationFactory, tenantTimeFactory)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            AchServiceFactory = achServiceFactory;
            LoggerFactory = loggerFactory;
            StatusManagementServiceFactory = statusManagementServiceFactory;
            ApplicationFilterClientFactory = applicationFilterClientFactory;
            ApplicationServiceClientFactory = applicationServiceClientFactory;
            MerchantCacheFactory = merchantCacheFactory;
            EventHubClientFactory = eventHubClientFactory;
            DataAttributesFactory = dataAttrbituesFactory;
        }

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private IAchServiceFactory AchServiceFactory { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IStatusManagementServiceFactory StatusManagementServiceFactory { get; }
        private IApplicationFilterClientFactory ApplicationFilterClientFactory { get; }
        private IApplicationServiceClientFactory ApplicationServiceClientFactory { get; }
        private IMerchantCacheServiceFactory MerchantCacheFactory { get;  }
        private IEventHubClientFactory EventHubClientFactory { get; }
        private IDataAttributesClientFactory DataAttributesFactory { get; }
        public override async void OnSchedule()
        {
            var token = TokenHandler.Issue(Tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);

            var achService = AchServiceFactory.Create(reader);
            var statusManagementService = StatusManagementServiceFactory.Create(reader);
            var applicationFilterService = ApplicationFilterClientFactory.Create(reader);

            var merchantService = MerchantCacheFactory.Create(reader);
            var applicationService = ApplicationServiceClientFactory.Create(reader);
            var eventHub = EventHubClientFactory.Create(reader);
            var dataAttributes = DataAttributesFactory.Create(reader);
            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            if (configurationService == null)
            {
                logger.Error("Was not found the Configuration related to fund applications task");
                return;
            }

            var configuration = configurationService.Get();
            if (configuration == null)
            {
                logger.Error("Was not found the Configuration related to fund applications task");
                return;
            }
            
            var fundingApprovedStatusCode = configuration.FundingApprovedStatusCodes;
            if (fundingApprovedStatusCode == null || !fundingApprovedStatusCode.Any())
            {
                logger.Error("Was not found any funding approved status code.");
                return;
            }

            var fundedStatus = configuration.FundedStatus;
            if (string.IsNullOrWhiteSpace(fundedStatus?.Code))
            {
                logger.Error("Was not found any funded status code.");
                return;
            }

            if (string.IsNullOrWhiteSpace(configuration.AchConfiguration?.FundingSourceId))
            {
                logger.Error("Was not Ach Configuration.");
                return;
            }
           
            logger.Info($"Get all applications that are {string.Join(",",configuration.FundingApprovedStatusCodes)} (Funding Approved)");
            var applications = (await applicationFilterService.GetByStatus(configuration.FundingApprovedStatusCodes)).ToList();
           
            logger.Info("End");
            if (applications.Any())
            {
                
                Parallel.ForEach(applications, application =>
                {
                    IInstruction instructionResponse=null;
                    try
                    {                                               
                        if (application.Payout > 0)
                        {
                            var existingStatus = statusManagementService.GetStatusByEntity("application", application.ApplicationId);
                            //TO Make sure we have same status in status-management, if there is any delay in filter it will have an issue that's why.
                            if (!configuration.FundingApprovedStatusCodes.Contains(existingStatus.Code,
                                StringComparer.OrdinalIgnoreCase))
                            {
                                logger.Info($"Application {application.ApplicationId} has different status in application filter and status management service.");
                                return;
                            }
                            var netFunding = dataAttributes.GetAttribute("application", application.ApplicationId, "netFunding").Result;

                            logger.Info($"Create instruction payload with the application {application.ApplicationId} Payout Amount");
                            var bankAccountDetails = GetReceiving(merchantService, applicationService, application);
                            var instructionRequest = new InstructionRequest
                            {
                                Amount = Convert.ToDecimal(netFunding),
                                Frequency = Frequency.Single,
                                Type = bankAccountDetails.Item2,
                                FundingSourceId = configuration.AchConfiguration.FundingSourceId,
                                StandardEntryClassCode = configuration.AchConfiguration.StandardEntryClassCode,
                                ReferenceNumber = application.ApplicationId,
                                Receiving = bankAccountDetails.Item1
                            };
                            logger.Info("End");

                            //Note: added validation as it's breaking in ACH.
                            if (instructionRequest.Receiving.RoutingNumber != null && instructionRequest.Receiving.RoutingNumber.Length != 9)
                                throw new Exception($"Invalid Routing number {instructionRequest.Receiving.RoutingNumber} for application {application.ApplicationId}");

                            logger.Info($"Start application {application.ApplicationId} to Queue the Instruction in the ACH service");
                            instructionResponse = achService.Queue(instructionRequest);
                            logger.Info("End");

                            logger.Info($"Start application {application.ApplicationId} to Change Status to {fundedStatus.Code} (Funded)");
                            statusManagementService.ChangeStatus("application", application.ApplicationId, fundedStatus.Code, fundedStatus.Reason);

                            logger.Info($"Application: {application.ApplicationId} has been successfully updated with status { fundedStatus.Code}.");

                            eventHub.Publish(new TaskApplicationFundSucceed(application.ApplicationId)).Wait();

                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error($"Failed attempt to fund application. Funding: {application.ApplicationId}", ex);

                        try
                        {
                            if (instructionResponse != null)
                                achService.Dequeue(instructionResponse.Id);

                            statusManagementService.ChangeStatus("application", application.ApplicationId, configuration.FundingReviewStatus.Code, configuration.FundingReviewStatus.Reason);

                            //To continue other application if one failed.
                            eventHub.Publish(new TaskApplicationFundFailed(application.ApplicationId)).Wait();

                        }
                        catch (Exception innerException)
                        {
                            logger.Error($"Failed attempt to fund application. Funding: {application.ApplicationId}", innerException);
                        }

                    }
                });
            }

            merchantService.Clear();
        }

        private Tuple<Receiving, TransactionType> GetReceiving(IMerchantCacheService merchantService, IApplicationService applicationService, IFilterView applicationFilterView)
        {
            if (applicationFilterView.SourceType.Equals("merchant", StringComparison.InvariantCultureIgnoreCase))
            {
                var merchantResponse = merchantService.Get(applicationFilterView.SourceId);
                if (merchantResponse == null)
                    throw new NotFoundException($"Merchant {applicationFilterView.SourceId} not found");

                var bankAccountDetails = merchantResponse.BankAccounts.FirstOrDefault(p => p.IsPrimary);
                if (bankAccountDetails == null)
                {
                    throw new NotFoundException("Bank account not found for the merchant");
                }

                var instructionType = bankAccountDetails.AccountType == Merchant.AccountType.Savings
                    ? TransactionType.CreditToSavings
                    : TransactionType.CreditToChecking;

                return new Tuple<Receiving, TransactionType>(new Receiving
                {
                    AccountNumber = bankAccountDetails.AccountNumber,
                    AccountType = AccountType.Corporate,
                    Name = bankAccountDetails.AccountHolderName,
                    RoutingNumber = bankAccountDetails.RoutingNumber
                }, instructionType);
            }

            if (applicationFilterView.SourceType.Equals("application", StringComparison.InvariantCultureIgnoreCase))
            {
                var application = applicationService.GetByApplicationNumber(applicationFilterView.Id);
                if (application == null)
                    throw new NotFoundException($"Application {applicationFilterView.Id} not found");

                var bankAccountDetails = application.BankAccounts.FirstOrDefault(p => p.IsPrimary);
                if (bankAccountDetails == null)
                {
                    throw new NotFoundException("Bank account not found for the merchant");
                }

                var instructionType = bankAccountDetails.AccountType == BankAccountType.Savings
                    ? TransactionType.CreditToSavings
                    : TransactionType.CreditToChecking;

                var primaryApplicant = application.Applicants.FirstOrDefault(applicant => applicant.IsPrimary);
                if (primaryApplicant == null)
                    throw new NotFoundException("Primary Applicant not found");

                return new Tuple<Receiving, TransactionType>(new Receiving
                {
                    AccountNumber = bankAccountDetails.AccountNumber,
                    AccountType = AccountType.Individual,
                    Name = primaryApplicant.FirstName + primaryApplicant.LastName,
                    RoutingNumber = bankAccountDetails.RoutingNumber
                }, instructionType);
            }

            throw new InvalidOperationException($"{applicationFilterView.SourceType} is not supported.");
        }

    }
}