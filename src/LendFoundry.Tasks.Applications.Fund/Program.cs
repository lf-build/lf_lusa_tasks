﻿using LendFoundry.Application.Client;
using LendFoundry.Applications.Filters.Client;
using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Merchant.Client;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement.Client;
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Tasks.Applications.Fund
{
    public class Program : DependencyInjection
    {
        public void Main(string[] args)
        {
            Provider.GetService<IAgent>().Execute();
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddCalendarService(Settings.Calendar.Host, Settings.Calendar.Port);
            services.AddAchService(Settings.Ach.Host, Settings.Ach.Port);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddConfigurationService<Configuration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddStatusManagementService(Settings.StatusManagement.Host, Settings.StatusManagement.Port);
            services.AddApplicationFilters(Settings.ApplicationFilter.Host, Settings.ApplicationFilter.Port);
            services.AddMerchant(Settings.Merchant.Host, Settings.Merchant.Port);
            services.AddApplicationService(Settings.Application.Host, Settings.Application.Port);
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddTransient<IAgent, Agent>();
            services.AddTransient<IMerchantCacheServiceFactory, MerchantCacheServiceFactory>();
            services.AddDataAttributes(Settings.DataAttributes.Host, Settings.DataAttributes.Port);
            return services;
        }
    }
}