namespace LendFoundry.Tasks.Applications.Fund
{
    public class Configuration
    {
        public string[] FundingApprovedStatusCodes { get; set; }
        public StatusConfiguration FundedStatus { get; set; }
        public AchConfiguration AchConfiguration { get; set; }
        public StatusConfiguration FundingReviewStatus { get; set; }
    }
}