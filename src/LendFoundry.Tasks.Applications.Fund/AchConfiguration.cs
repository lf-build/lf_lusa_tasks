namespace LendFoundry.Tasks.Applications.Fund
{
    public class AchConfiguration
    {
        public string FundingSourceId { get; set; }
        public string StandardEntryClassCode { get; set; }
    }
}