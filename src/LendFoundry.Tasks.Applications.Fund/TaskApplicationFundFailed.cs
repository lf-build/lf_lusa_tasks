namespace LendFoundry.Tasks.Applications.Fund
{
    public class TaskApplicationFundFailed
    {
        public TaskApplicationFundFailed()
        {

        }

        public TaskApplicationFundFailed(string applicationNumber)
        {
            ApplicationNumber = applicationNumber;
        }
        public string ApplicationNumber { get; set; }
    }
}