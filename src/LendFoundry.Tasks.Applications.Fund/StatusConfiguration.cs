namespace LendFoundry.Tasks.Applications.Fund
{
    public class StatusConfiguration
    {
        public string Code { get; set; }
        public string Reason { get; set; }
    }
}