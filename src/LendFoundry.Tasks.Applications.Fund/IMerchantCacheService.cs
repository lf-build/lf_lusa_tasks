using LendFoundry.Merchant;

namespace LendFoundry.Tasks.Applications.Fund
{
    public interface IMerchantCacheService
    {
        IMerchant Get(string merchantId);
        void Clear();
    }
}