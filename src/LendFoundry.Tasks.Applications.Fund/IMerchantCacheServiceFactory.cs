using LendFoundry.Security.Tokens;

namespace LendFoundry.Tasks.Applications.Fund
{
    public interface IMerchantCacheServiceFactory
    {
        IMerchantCacheService Create(ITokenReader reader);
    }
}