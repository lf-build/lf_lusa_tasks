namespace LendFoundry.Tasks.Applications.Fund
{
    public class TaskApplicationFundSucceed
    {
        public TaskApplicationFundSucceed()
        {
            
        }

        public TaskApplicationFundSucceed(string applicationNumber)
        {
            ApplicationNumber = applicationNumber;
        }

        public string ApplicationNumber { get; set; }
    }
}