using System;
using LendFoundry.Foundation.Services.Settings;

namespace LendFoundry.Tasks.Applications.Fund
{
    public static class Settings
    {
        public const string Prefix = "TASK_APPLICATIONS_FUND";

        public static string ServiceName { get; } = "task-applications-fund";

        public static ServiceSettings Calendar { get; } = new ServiceSettings($"{Prefix}_CALENDAR_HOST", "calendar", $"{Prefix}_CALENDAR_PORT");

        public static ServiceSettings StatusManagement { get; } = new ServiceSettings($"{Prefix}_STATUSMANAGEMENT_HOST", "status-management", $"{Prefix}_STATUSMANAGEMENT_PORT");

        public static ServiceSettings ApplicationFilter { get; } = new ServiceSettings($"{Prefix}_APPLICATIONFILTER_HOST", "application-filters", $"{Prefix}_APPLICATIONFILTER_PORT");

        public static ServiceSettings Ach { get; } = new ServiceSettings($"{Prefix}_ACH_HOST", "ach", $"{Prefix}_ACH_PORT");
        
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");

        public static ServiceSettings Merchant { get; } = new ServiceSettings($"{Prefix}_MERCHANT_HOST", "merchant", $"{Prefix}_MERCHANT_PORT");

        public static ServiceSettings Application { get; } = new ServiceSettings($"{Prefix}_APPLICATION_HOST", "application", $"{Prefix}_APPLICATION_PORT");
        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");
        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats://nats:4222";
        public static ServiceSettings DataAttributes { get; } = new ServiceSettings($"{Prefix}_DATAATTRIBUTE_HOST", "data-attribute", $"{Prefix}_DATAATTRIBUTE_PORT");

    }
}