﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Tasks.Applications.SendEmailToCreateAccount
{
    public class Settings
    {
        public const string Prefix = "TASK_SEND_EMAIL_TO_CREATE_ACCOUNT";

        public static string ServiceName { get; } = Prefix.ToLower();

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");

        public static ServiceSettings Application { get; } = new ServiceSettings($"{Prefix}_APPLICATION_HOST", "application", $"{Prefix}_APPLICATION_PORT");

        public static ServiceSettings Applicant { get; } = new ServiceSettings($"{Prefix}_APPLICANT_HOST", "applicant", $"{Prefix}_APPLICANT_PORT");

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB_HOST", "eventhub", $"{Prefix}_EVENTHUB_PORT");

        public static ServiceSettings Email { get; } = new ServiceSettings($"{Prefix}_EMAIL_HOST","email", $"{Prefix}_EMAIL_PORT");

        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats://nats:4222";

        public static string EventName = Environment.GetEnvironmentVariable("TASK_EVENT_NAME");
    }
}