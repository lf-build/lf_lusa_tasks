﻿
namespace LendFoundry.Tasks.Applications.SendEmailToCreateAccount
{
    public class SendEmailToCreateAccountConfiguration
    {
        public string EntityId { get; set; }

        public string EntityType { get; set; }

        public string EventName { get; set; } = "ApplicationCreated";

        public string TemplateName { get; set; }

        public string TemplateVersion { get; set; }

        public string Link { get; set; }
    }
}