﻿using LendFoundry.Application.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.Email.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Tasks.Applications.SendEmailToCreateAccount
{
    public class Program : DependencyInjection
    {
        public void Main()
        {
            Provider.GetService<IAgent>().Execute();
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddApplicationService(Settings.Application.Host, Settings.Application.Port);
            services.AddApplicantService(Settings.Applicant.Host, Settings.Applicant.Port);
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddEmailService(Settings.Email.Host, Settings.Email.Port);
            services.AddConfigurationService<SendEmailToCreateAccountConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddTransient<IAgent, Agent>();

            return services;
        }
    }
}