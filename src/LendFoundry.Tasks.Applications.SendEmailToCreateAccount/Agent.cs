﻿using LendFoundry.Application.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.Email.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Agent;
using System;
using System.Linq;

namespace LendFoundry.Tasks.Applications.SendEmailToCreateAccount
{
    public class Agent : EventBasedAgent
    {
        private IApplicationServiceClientFactory ApplicationServiceFactory { get; }
        private ITokenReaderFactory TokenReaderFactory { get; }
        private IEmailServiceFactory EmailServiceFactory { get; }
        private IApplicantServiceFactory ApplicantServiceFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }

        public Agent
        (
            IEventHubClientFactory factory,
            IApplicationServiceClientFactory applicationServiceFactory,
            ITokenReaderFactory tokenReaderFactory,
            IEmailServiceFactory emailServiceFactory,
            IApplicantServiceFactory applicantServiceFactory,
            ILoggerFactory loggerFactory,
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory,
            IConfigurationServiceFactory configurationFactory = null

        ) : base(factory, Settings.ServiceName, Settings.EventName, tokenHandler, loggerFactory)
        {
            ApplicationServiceFactory = applicationServiceFactory;
            ApplicantServiceFactory = applicantServiceFactory;
            TokenReaderFactory = tokenReaderFactory;
            EmailServiceFactory = emailServiceFactory;
            ApplicantServiceFactory = applicantServiceFactory;
            ConfigurationFactory = configurationFactory;
            TenantTimeFactory = tenantTimeFactory;
        }

        public async override void Execute(EventInfo @event)
        {
            var reader = TokenReaderFactory.Create(@event.TenantId, string.Empty);
            var applicationService = ApplicationServiceFactory.Create(reader);
            var emailService = EmailServiceFactory.Create(reader);
            var applicantService = ApplicantServiceFactory.Create(reader);
            var logger = LoggerFactory.CreateLogger();
            
            try
            {
                var configurationService = ConfigurationFactory.Create<SendEmailToCreateAccountConfiguration>(Settings.ServiceName, reader);
                if (configurationService == null)
                {
                    logger.Error($"Did not find the configuration related to {Settings.ServiceName}");
                    return;
                }

                var configuration = configurationService.Get();
                if (configuration == null)
                {
                    logger.Error($"The configuration related to {Settings.ServiceName} was not found");
                    return;
                }

                var entityType = configuration.EntityType;
                var entityId = configuration.EntityId.FormatWith(@event);

                logger.Info($"Received request to EntityType: {entityType}, and EntityId: {entityId}");

                if ("application".Equals(entityType, StringComparison.InvariantCultureIgnoreCase) || string.IsNullOrWhiteSpace(entityId))
                {
                    var application = applicationService.GetByApplicationNumber(entityId);

                    if (application == null)
                    {
                        logger.Warn($"Application {entityId} not found.");
                        return;
                    }

                    var applicant = applicantService.Get(application.Applicants.FirstOrDefault(m => m.IsPrimary).Id);
                    var token = applicantService.CreateIdentityToken(applicant.Id);
                    var link = configuration.Link.FormatWith(new { token });

                    var request = new
                    {
                        templateName = configuration.TemplateName,
                        templateVersion = configuration.TemplateVersion,
                        email = applicant.Email,
                        borrowerName = $"{applicant.FirstName} {applicant.LastName}",
                        customerEmail = applicant.Email,
                        firstName = applicant.FirstName,
                        lastName = applicant.LastName,
                        link = link
                    };

                    await emailService.Send(configuration.TemplateName, configuration.TemplateVersion, request);

                    logger.Info($"Notification sent to {applicant.Email} - {entityType}: {entityId}");
                }
            }
            catch (Exception ex)
            {
                logger.Error($"Exception on event {@event.Id} - {@event.Name}", ex);
            }
        }
    }
}