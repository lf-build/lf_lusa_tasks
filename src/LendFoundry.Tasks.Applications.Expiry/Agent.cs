using LendFoundry.Merchant.Filters.Client;
using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement.Client;
using LendFoundry.Tasks.Agent;
using System;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Configuration;

namespace LendFoundry.Tasks.Applications.Expiry
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ITenantTimeFactory tenantTimeFactory,
            IStatusManagementServiceFactory statusManagementServiceFactory,
             ICalendarServiceFactory calendarServiceFactory,
            IApplicationFilterClientFactory applicationFilterClientFactory,
            ILoggerFactory loggerFactory)
            : base(tokenHandler, configurationFactory, tenantTimeFactory)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            CalendarServiceFactory = calendarServiceFactory;
            StatusManagementServiceFactory = statusManagementServiceFactory;
            ApplicationFilterClientFactory = applicationFilterClientFactory;
            LoggerFactory = loggerFactory;
            EventHubFactory = eventHubFactory;
        }

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ICalendarServiceFactory CalendarServiceFactory { get; }
        private IStatusManagementServiceFactory StatusManagementServiceFactory { get; }
        private IApplicationFilterClientFactory ApplicationFilterClientFactory { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }

        public override async void OnSchedule()
        {
            var token = TokenHandler.Issue(Tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var calendar = CalendarServiceFactory.Create(reader);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var statusManagement = StatusManagementServiceFactory.Create(reader);
            var applicationFilter = ApplicationFilterClientFactory.Create(reader);
            var eventHub = EventHubFactory.Create(reader);

            var configuration = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            if (configuration == null)
            {
                logger.Error("Was not found the Configuration related to application filter expiry status Task");
                return;
            }

            var applicationsExpiryConfiguration = configuration.Get();
            if (applicationsExpiryConfiguration == null)
            {
                logger.Error("Was not found the configuration related to applications expiry");
                return;
            }
            if (applicationsExpiryConfiguration.Expiration == null)
            {
                logger.Error("Was not found the configuration related to applications expiry status");
                return;
            }

            var expiryStatusCode = applicationsExpiryConfiguration.Expiration.StatusCode;
            if (string.IsNullOrWhiteSpace(expiryStatusCode))
            {
                logger.Warn("Was not found any status code.");
                return;
            }
            try
            {
                var expiredApplications = await applicationFilter.GetAllExpiredApplications();
                if (expiredApplications == null || !expiredApplications.Any())
                {
                    logger.Info("No application has been expired yet.");
                    return;
                }

                logger.Info("Start applications expiry  task");

                Parallel.ForEach(expiredApplications, application =>
                {
                    try
                    {
                        if (!string.IsNullOrWhiteSpace(application.ApplicationId))
                        {
                            statusManagement.ChangeStatus("application", application.ApplicationId, expiryStatusCode, string.Empty);
                            eventHub.Publish("TaskApplicationsExpiryCalled", new { ExpiredApplication = application }).Wait();
                            logger.Info($"Application: {application.ApplicationId} has been successfully updated with status {expiryStatusCode}.");
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error($"Failed attempt to update application status. Application: {application.ApplicationId}\n The status management service is returning this error:", ex);
                    }
                });
                logger.Info("End");
            }
            catch (Exception ex)
            {
                logger.Error($"Failed attempt to update application status. Application Filter service not started yet.", ex);
            }
        }
    }
}