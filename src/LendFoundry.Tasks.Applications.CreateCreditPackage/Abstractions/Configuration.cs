using System.Collections.Generic;
namespace LendFoundry.Tasks.Applications.CreateCreditPackage
{
    public class Configuration
    {
        public string FundedStatusCode { get; set; }
        public Dictionary<string,bool> Files { get; set; }
        public CertificateOfCompletionTemplate CertificateOfCompletionTemplate { get; set; }
        public string CreditPackageFileName { get; set; }
    }
}