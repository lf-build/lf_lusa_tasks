using System.Collections.Generic;
using System.IO;

namespace LendFoundry.Tasks.Applications.CreateCreditPackage
{
    public interface IPdfMergerService
    {
        byte[] MergePdf(IEnumerable<Stream> streams);
    }
}