using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Tasks.Applications.CreateCreditPackage
{
    public static class Settings
    {
        public const string Prefix = "TASK_APPLICATIONS_CREATECREDITPACKAGE";

        public static string ServiceName { get; } = Prefix.ToLower();

        public static ServiceSettings Application { get; } = new ServiceSettings($"{Prefix}_APPLICATION_HOST", "application", $"{Prefix}_APPLICATION_PORT");

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");

        public static ServiceSettings DocumentManager { get; } = new ServiceSettings($"{Prefix}_DOCUMENTMANAGER_HOST", "document-manager", $"{Prefix}_DOCUMENTMANAGER_PORT");

        public static ServiceSettings DocumentGenerator { get; } = new ServiceSettings($"{Prefix}_DOCUMENTGENERATOR_HOST", "document-generator", $"{Prefix}_DOCUMENTGENERATOR_PORT");

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB_HOST", "eventhub", $"{Prefix}_EVENTHUB_PORT");

        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats://nats:4222";
        public static ServiceSettings DataAttributes { get; } = new ServiceSettings($"{Prefix}_DATAATTRIBUTE_HOST", "data-attribute", $"{Prefix}_DATAATTRIBUTE_PORT");

        public static string SubscribedEventName => Environment.GetEnvironmentVariable($"{Prefix}_SUBSCRIBE_EVENT_NAME") ?? "ApplicationStatusChanged";
    }
}