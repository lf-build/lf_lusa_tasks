using System.Collections.Generic;

namespace LendFoundry.Tasks.Applications.CreateCreditPackage
{
    public class ApplicationCreditPackageFailed
    {
        public string ApplicationNumber { get; set; }
        public List<string> MissingDocuments { get; set; }
    }
}