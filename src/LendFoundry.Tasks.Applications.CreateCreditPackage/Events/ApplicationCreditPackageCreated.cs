﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Tasks.Applications.CreateCreditPackage
{
    public class ApplicationCreditPackageCreated
    {
        public string ApplicationNumber { get; set; }
    }
}
