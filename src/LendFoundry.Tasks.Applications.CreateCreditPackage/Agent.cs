using LendFoundry.Merchant.Application;
using LendFoundry.DocumentGenerator;
using LendFoundry.DocumentManager;
using LendFoundry.DocumentManager.Client;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement;
using LendFoundry.Tasks.Agent;
using LendFoundry.TemplateManager;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Configuration;
using LendFoundry.DataAttributes.Client;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Merchant.Application.Client;
using LendFoundry.Tenant.Client;
using Document = LendFoundry.DocumentGenerator.Document;
using IDocument = LendFoundry.DocumentManager.IDocument;
using IDocumentGeneratorServiceFactory = LendFoundry.DocumentGenerator.Client.IDocumentGeneratorServiceFactory;
using ILogger = LendFoundry.Foundation.Logging.ILogger;
using ILoggerFactory = LendFoundry.Foundation.Logging.ILoggerFactory;

namespace LendFoundry.Tasks.Applications.CreateCreditPackage
{
    public class Agent : EventBasedAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IDocumentGeneratorServiceFactory documentGeneratorServiceFactory,
            IApplicationServiceClientFactory applicationServiceClientFactory,
            IEventHubClientFactory eventHubFactory,
            IDocumentManagerServiceFactory documentManagerServiceFactory,
            IPdfMergerService pdfMergerService,
            ITokenReaderFactory tokenReaderFactory,
            IDataAttributesClientFactory dataAttributesClientFactory
        )
            : base(eventHubFactory, tenantServiceFactory, configurationFactory, Settings.ServiceName,
                Settings.SubscribedEventName, tokenHandler, loggerFactory) 
        {
            ConfigurationFactory = configurationFactory;
            DocumentGeneratorServiceFactory = documentGeneratorServiceFactory;
            ApplicationServiceClientFactory = applicationServiceClientFactory;
            DocumentManagerServiceFactory = documentManagerServiceFactory;
            PdfMergerService = pdfMergerService;
            TokenReaderFactory = tokenReaderFactory;
            DataAttributesClientFactory = dataAttributesClientFactory;
        }

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private IDocumentGeneratorServiceFactory DocumentGeneratorServiceFactory { get; }
        private IApplicationServiceClientFactory ApplicationServiceClientFactory { get; }
        private IDocumentManagerServiceFactory DocumentManagerServiceFactory { get; }
        private IPdfMergerService PdfMergerService { get; set; }
        private ITokenReaderFactory TokenReaderFactory { get; }
        private IDataAttributesClientFactory DataAttributesClientFactory { get; }

        public override async void Execute(EventInfo @event)
        {
            var reader = TokenReaderFactory.Create(@event.TenantId, string.Empty);
            var logger = LoggerFactory.CreateLogger();
            logger.Info($"starting process for eventId {@event.Id}");
            IEventHubClient eventHubClient = null;
            IApplicationResponse application = null;

            try
            {
                var statusChangedEvent = EnsureValidEvent(@event, logger);
                if (statusChangedEvent == null)
                {
                    return;
                }

                var configuration = GetConfiguration(reader, logger);
                if (configuration == null)
                {
                    logger.Error("Was not found the Configuration related to create credit package task");
                    return;
                }

                if (statusChangedEvent.NewStatus != configuration.FundedStatusCode)
                {
                    logger.Error($"Status code is not {configuration.FundedStatusCode}");
                    return;
                }

                var applicationService = ApplicationServiceClientFactory.Create(reader);
                var documentService = DocumentManagerServiceFactory.Create(reader);
                eventHubClient = EventHubFactory.Create(reader);
                application = applicationService.GetByApplicationNumber(statusChangedEvent.EntityId);

                if (application == null)
                {
                    logger.Error($"Application {statusChangedEvent.EntityId} not found");
                    return;
                }

                logger.Info($"Started proceessing for {application.ApplicationNumber}");
                var creditPackageFileName = configuration.CreditPackageFileName.FormatWith(new { application.ApplicationNumber });

                logger.Info("Start to get all documents(their information) from Document Manager");
                var documents = await documentService.GetAllLatest("application", application.ApplicationNumber);
                var packageDocuments = await FilterDocumentsToPackage(application.ApplicationNumber, documents, configuration, eventHubClient);

                logger.Info("End get all documents");
                if (packageDocuments == null || !packageDocuments.Any())
                {
                    logger.Error($"Required documents not found for application {application.ApplicationNumber}");
                    return;
                }

                logger.Info("Create Certificate of Completion using the information retrieved");
                var documentGeneratorService = DocumentGeneratorServiceFactory.Create(reader);
                var dataAttributeService = DataAttributesClientFactory.Create(reader);
                var activityItems=dataAttributeService.GetAttribute("application", application.ApplicationNumber,
                    configuration.CertificateOfCompletionTemplate.ActivityAttributeNames.Keys.ToList()).Result;

                var activityList = new Dictionary<string, DateTimeOffset>();
                foreach (var activityAttribute in configuration.CertificateOfCompletionTemplate.ActivityAttributeNames)
                {
                    if (!activityItems.ContainsKey(LowercaseFirst(activityAttribute.Key)))
                        logger.Warn("Attribute value not found:" + activityAttribute.Key);
                    else
                        activityList.Add(activityAttribute.Value, DateTimeOffset.Parse(activityItems[LowercaseFirst(activityAttribute.Key)].ToString()));
                }

                var certificateOfCompletionData = new CertificateOfCompletionPayload(application, activityList, @event.IpAddress);

                var certificateOfCompletionResponse =
                    await
                        documentGeneratorService.Generate(configuration.CertificateOfCompletionTemplate.Name,
                            configuration.CertificateOfCompletionTemplate.Version, Format.Html, new Document()
                            {
                                Data = certificateOfCompletionData,
                                Format = DocumentFormat.Pdf,
                                Name = creditPackageFileName,
                                Version = "1.0"
                            });

                if (certificateOfCompletionResponse?.Content == null ||
                    certificateOfCompletionResponse.Content.Length <= 0)
                {
                    logger.Error("Unable to generate Certificate of Completion");
                    return;
                }

                var streams = GetStreamsToMerge(application, packageDocuments, documentService, certificateOfCompletionResponse);
                var mergedPdf = PdfMergerService.MergePdf(streams);

                using (var outputStream = new MemoryStream(mergedPdf))
                {
                    var certificateOfCompletionDocument =
                        await documentService.Create(outputStream, "application", application.ApplicationNumber,
                            creditPackageFileName + ".pdf");

                    if (string.IsNullOrWhiteSpace(certificateOfCompletionDocument?.Id))
                    {
                        logger.Error("Unable to upload Certificate of Completion document");
                    }
                    else
                    {
                        await
                            eventHubClient.Publish(new ApplicationCreditPackageCreated()
                            {
                                ApplicationNumber = application.ApplicationNumber
                            });

                        logger.Info($"Successfully processed for {application.ApplicationNumber}");
                    }
                }
            }
            catch (Exception ex)
            {
                if (application != null && eventHubClient != null)
                {
                    await eventHubClient.Publish(new ApplicationCreditPackageFailed
                    {
                        ApplicationNumber = application.ApplicationNumber
                    });
                }

                logger.Error($"An exception occured in {Settings.ServiceName} with message: {ex.Message}", ex);
            }

            logger.Info($"Finished processed for eventId {@event.Id}");

        }

        private static string LowercaseFirst(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return string.Empty;
            }
            return $"{char.ToLower(text[0])}{text.Substring(1)}";
        }

        private static IEnumerable<Stream> GetStreamsToMerge(IApplicationDetail application, IEnumerable<IDocument> packageDocuments, IDocumentManagerService documentManagerService, IDocumentResult certificateOfCompletionResponse)
        {
            foreach (var document in packageDocuments)
            {
                yield return
                    documentManagerService.Download("application", application.ApplicationNumber, document.Id).Result;
            }

            using (var certificateOfCompletionStream = new MemoryStream(certificateOfCompletionResponse.Content))
            {
                yield return certificateOfCompletionStream;
            }
        }

        private Configuration GetConfiguration(ITokenReader reader, ILogger logger)
        {
            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            if (configurationService == null)
            {
                return null;
            }

            var configuration = configurationService.Get();

            return EnsureConfigurationIsValid(configuration, logger) ? configuration : null;
        }

        private StatusChanged EnsureValidEvent(EventInfo @event, ILogger logger)
        {
            if (@event.Data == null)
            {
                logger.Error($"Cast to {nameof(StatusChanged)} cannot is possible");
                return null;
            }
            var statusChangedEvent = @event.Cast<StatusChanged>();

            if (!"application".Equals(statusChangedEvent.EntityType, StringComparison.InvariantCultureIgnoreCase))
            {
                logger.Error($"Skipping for Entity Type {statusChangedEvent.EntityType}. Only application entityType is allowed.");
                return null;
            }

            if (string.IsNullOrWhiteSpace(statusChangedEvent.EntityId))
            {
                logger.Error("EntityId is null or empty");
                return null;
            }

            return statusChangedEvent;
        }

        private static async Task<List<IDocument>> FilterDocumentsToPackage(string applicationNumber, IEnumerable<IDocument> documents, Configuration configuration, IEventHubClient eventHubClient)
        {
            var documentsToPackage = new List<IDocument>();

            if (documents == null)
            {
                return null;
            }

            var documentList = documents.ToList();
            var missingRequiredFiles = new List<string>();
            foreach (var packageFile in configuration.Files)
            {
                var fileName = packageFile.Key.FormatWith(new { ApplicationNumber = applicationNumber });
                var documentToPackage = documentList.FirstOrDefault(p => p.FileName.Equals(fileName, StringComparison.InvariantCultureIgnoreCase));
                if (packageFile.Value && documentToPackage == null)
                {
                    missingRequiredFiles.Add(fileName);
                }
                if (documentToPackage != null)
                    documentsToPackage.Add(documentToPackage);
            }

            if (missingRequiredFiles.Any())
            {
                await eventHubClient.Publish(new ApplicationCreditPackageFailed
                {
                    ApplicationNumber = applicationNumber,
                    MissingDocuments = missingRequiredFiles
                });
                return null;
            }

            return documentsToPackage;
        }

        private bool EnsureConfigurationIsValid(Configuration configuration, ILogger logger)
        {
            if (configuration == null)
            {
                logger.Error($"Was not found the Configuration related to {Settings.ServiceName}");
                return false;
            }

            if (configuration.Files == null || !configuration.Files.Any())
            {
                logger.Error("Was not found any required files.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(configuration.FundedStatusCode))
            {
                logger.Error("Was not found funded status code.");
                return false;
            }

            if (configuration.CertificateOfCompletionTemplate == null)
            {
                logger.Error("Was not found the certificate of completion template related to create credit package task");
                return false;
            }

            if (string.IsNullOrWhiteSpace(configuration.CertificateOfCompletionTemplate.Name) || string.IsNullOrWhiteSpace(configuration.CertificateOfCompletionTemplate.Version))
            {
                logger.Error("Was not found the certificate of completion template related to create credit package task");
                return false;
            }

            return true;
        }
    }
}