﻿using System.Collections.Generic;

namespace LendFoundry.Tasks.Applications.CreateCreditPackage
{
    public class CertificateOfCompletionTemplate
    {
        public string Name { get; set; }
        public string Version { get; set; }
        public Dictionary<string,string> ActivityAttributeNames { get; set; }
    }
}