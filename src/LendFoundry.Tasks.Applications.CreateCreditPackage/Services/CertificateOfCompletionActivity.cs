﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Tasks.Applications.CreateCreditPackage
{
    public class CertificateOfCompletionActivity 
    {
        public CertificateOfCompletionActivity(KeyValuePair<string, DateTimeOffset> document)
        {
            Name = document.Key;
            CompletionDate = document.Value.ToString("MMMM dd, yyyy hh:mm tt");
        }

        public string Name { get; set; }
        public string CompletionDate { get; set; }
    }
}