﻿using System;
using LendFoundry.Merchant.Application;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Tasks.Applications.CreateCreditPackage
{
    public class CertificateOfCompletionPayload
    {
        public CertificateOfCompletionPayload(IApplicationResponse applicationResponse, Dictionary<string, DateTimeOffset> activities, string ipAddress)
        {
            if (applicationResponse == null)
                throw new ArgumentNullException(nameof(applicationResponse));

            if (activities == null)
                throw new ArgumentNullException(nameof(activities));

            if (activities.Count == 0)
                throw new ArgumentException("Value cannot be an empty collection.", nameof(activities));

            var primaryApplicant = applicationResponse.Applicants.FirstOrDefault(p => p.IsPrimary);
            if (primaryApplicant != null)
            {
                BorrowerName = $"{primaryApplicant.FirstName} {primaryApplicant.LastName}";
                BorrowerEmail = primaryApplicant.Email;
            }

            ApplicationDate = applicationResponse.SubmittedDate.Time.ToString("MMMM dd, yyyy");
            LoanNumber = applicationResponse.ApplicationNumber;
            DateCreated = applicationResponse.SubmittedDate.Time.ToString("MMMM dd, yyyy hh:mm:ss tt");
            Documents = activities.Select(p => new CertificateOfCompletionActivity(p));
            IpAddress = ipAddress.Replace("::ffff:", string.Empty);
        }

        public string BorrowerName { get; set; }
        public string BorrowerEmail { get; set; }
        public string ApplicationDate { get; set; }
        public string LoanNumber { get; set; }
        public string DateCreated { get; set; }
        public string IpAddress {get;set;}
        public IEnumerable<CertificateOfCompletionActivity> Documents { get; set; }
    }
}