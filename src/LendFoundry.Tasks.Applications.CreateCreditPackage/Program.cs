﻿using LendFoundry.Configuration.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.DocumentGenerator.Client;
using LendFoundry.DocumentManager.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Merchant.Application.Client;
using LendFoundry.Security.Tokens;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Tasks.Applications.CreateCreditPackage
{
    public class Program : DependencyInjection
    {
        public static void Main(string[] args)
        {
            new Program().Provider.GetService<IAgent>().Execute();
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddConfigurationService<Configuration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddDocumentManager(Settings.DocumentManager.Host, Settings.DocumentManager.Port);
            services.AddDocumentGenerator(Settings.DocumentGenerator.Host, Settings.DocumentGenerator.Port);
            services.AddApplicationService(Settings.Application.Host, Settings.Application.Port);
            services.AddDataAttributes(Settings.DataAttributes.Host, Settings.DataAttributes.Port);

            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddTransient<IAgent, Agent>();
            services.AddTransient<IPdfMergerService, PdfMergerService>();
            return services;
        }
    }
}