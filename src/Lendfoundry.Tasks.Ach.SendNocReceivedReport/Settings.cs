﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace Lendfoundry.Tasks.Ach.SendNocReceivedReport
{
    public class Settings
    {
        public const string Prefix = "TASKS_ACH_SEND_NOC_RECEIVED_REPORT";

        public static string ServiceName { get; } = Prefix.ToLower();

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");

        public static ServiceSettings Ach { get; } = new ServiceSettings($"{Prefix}_ACH_HOST", "ach", $"{Prefix}_ACH_PORT");

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB_HOST", "eventhub", $"{Prefix}_EVENTHUB_PORT");

        public static ServiceSettings Email { get; } = new ServiceSettings($"{Prefix}_EMAIL_HOST","email", $"{Prefix}_EMAIL_PORT");

        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats://nats:4222";

        //public static string EventName = Environment.GetEnvironmentVariable("TASK_EVENT_NAME");
    }
}