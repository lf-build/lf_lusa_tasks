﻿using LendFoundry.Configuration.Client;
using LendFoundry.Email.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks;
#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

#else
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;

#endif
using System;

namespace Lendfoundry.Tasks.Ach.SendNocReceivedReport
{
    public class Program : LendFoundry.Tasks.DependencyInjection
    {
        public static void Main(string[] args)
        {

            Console.WriteLine("Application started");

            Program p = new Program();
            p.Provider.GetService<IAgent>().Execute();
            Console.WriteLine("Application terminated");

        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddAchService(Settings.Ach.Host, Settings.Ach.Port);
            services.AddEmailService(Settings.Email.Host, Settings.Email.Port);
            services.AddConfigurationService<SendNocReceivedReportConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddTransient<IAgent, Agent>();

            return services;
        }
    }
}